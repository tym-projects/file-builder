# File Builder  
File builder is a multiple file types creator class  

1. Icaa file implementation for send cinema data to icaa  
2. Icaa file incident for send inciden data weekly  
3. GnuPG class for encrypt file data with the public icaa certificate *.pkr  

## Install with composer  
```
composer config repositories.gitlab.com/12098683 '{"type": "composer", "url": "https://gitlab.com/api/v4/group/12098683/-/packages/composer/packages.json"}'

composer require tym-projects/file-builder
```

#### Requitites  
The encrypter class needs the 'gnupg' php extension to be executed  

#### Install GnuPG  
```
sudo apt update  
sudo apt -y install gcc make autoconf libc-dev pkg-config  
sudo apt -y install libgpgme11-dev php-dev
  
sudo pecl install gnupg  
sudo bash -c "echo extension=gnupg.so > $PHP_INI_PATH"  
sudo service apache2 restart  
```  
* if you are using PHPENV for multiple php versions you need to create a link to gnupg.so file  

```
sudo ln -s /usr/lib/php/{PHP_FOLDER}/gnupg.so /home/{USER}/.phpenv/versions/{PHP_VERSION}/lib/php/extensions/no-debug-*****/gnupg.so  
```  

#### Verify  
```  
php -i | grep gnupg  
```  
  
#### Pecl (pecl: command not found)  
Install pecl with this command:
```  
sudo apt install php-pear -y
```  
#### Phpize (phpize: not found)  
Install phpize with this command:
```  
sudo apt install php-dev -y
```  
## Example ICAA File
```
require_once 'vendor/autoload.php';

try {
    
    $config = [
        'box_code' => '879',
        'file_type' => ICAAFile::USUAL_FILE,
        'date_last' => '2021-05-12',
        'date_current' => '2021-05-21',
    ];

    //Create icaa file with the icaa file generator
    $file = (new ICAAFileGenerator(
        new ICAADataProvider())
    )->generate($config);

    $builder = new FileBuilder(
        new LocalFileSystem(),
        $file
    );
    $builder->saveFile('file.example');

} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}
```

## Example ICAA Incident File
```
require_once 'vendor/autoload.php';

try {
    
    $config = [
        'box_code' => '879',
        'initial_date' => '2021-05-17',
        'final_date' => '2021-05-23',
    ];

    $file = (new ICAAFileIncidentGenerator(
        new ICAADataProvider())
    )->generate($config);

    $builder = new FileBuilder(
        new LocalFileSystem(),
        $file
    );
    $builder->saveFile($file->getFileName());
} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}
```
## Example Encypt File  
```
require_once 'vendor/autoload.php';

try {
    
    /** if you have icaa pkr install in your system */
    $gpg = new GnuPGEncrypter();
    $array = $gpg->getKeyInfo('ICAA');

    /** if you need to install pkr */
    $gpg = new GnuPGEncrypter('path_to_file');
    $array = $gpg->getKeyInfo('ICAA');

    $original_content = file_get_contents('file.example');
    $fingerprint = $array[0]['subkeys'][0]['fingerprint'];

    $content = $gpg->encrypt($original_content,$fingerprint);
    
    file_put_contents("file.example.gpg", $content);

} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}
```
