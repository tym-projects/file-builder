<?php declare (strict_types = 1);

namespace FileBuilder;

use FileBuilder\File\FileType;
use FileBuilder\Storage\StorageSystem;

class FileBuilder
{
    private $storage;
    private $file;

    /**
     * @param StorageSystem $storage
     * @param FileType $file
     * @testFunction testFileBuilder
     */
    public function __construct(
        StorageSystem $storage,
        FileType $file
    ) {
        $this->storage = $storage;
        $this->file = $file;
    }

    /**
     * @param string $fileName
     * @return mixed
     */
    public function saveFile(string $fileName = "")
    {
        if (empty($fileName)) {
            $fileName = uniqid();
        }
        return $this->storage->save(
            $fileName,
            $this->file->generateContent()
        );
    }
}
