<?php declare (strict_types = 1);

namespace FileBuilder;

use FileBuilder\Exception\EncryptFileException;

class Signature
{

    private $cert;
    private $pkey;
    private $encryptKey;

    /**
     * Encrypter constructor.
     * @param string $encryptionKeyData
     * @param string $encryptKeyFingerprint
     */
    public function __construct(string $encryptionCert, string $encryptKey)
    {
        if (!is_file($encryptionCert)) {
            throw new EncryptFileException(sprintf("Certificate File not found in %s", EncryptFile::class));
        }
        if (!openssl_pkcs12_read($encryptionCert, $info, $encryptKey)) {
            throw new EncryptFileException(sprintf("Error getting certificate data in %s", EncryptFile::class));
        }

        $this->cert = $info['cert'];
        $this->pkey = $info['pkey'];
        $this->encryptKey = $encryptKey;
    }

    /**
     * @param string $content
     * @return string
     */
    public function sing(string $content): string
    {
        return $content;
    }

    /**
     * @param string $content
     * @return string
     */
    public function validateSing(string $content): string
    {
        return $content;
    }
}
