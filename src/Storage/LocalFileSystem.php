<?php

declare (strict_types = 1);

namespace FileBuilder\Storage;

use Exception;
use InvalidArgumentException;

/**
 * @testFunction testLocalFileSystem
 */
final class LocalFileSystem implements StorageSystem
{

    private $path;

    /**
     * Construct
     *
     * @param $path string
     */
    public function __construct(string $path = __DIR__ . "/../../")
    {
        if (!is_dir($path)) {
            throw new InvalidArgumentException(sprintf("Path %s not exist", $path));
        }
        $this->path = $path;
    }

    /**
     * save function
     *
     * @param string $file
     * @return boolean
     */
    public function save(string $fileName, string $fileContent): bool
    {
        if (is_file($this->path . $fileName)) {
            throw new Exception(sprintf("file %s exists", $fileName));
        }
        file_put_contents($this->path . $fileName, $fileContent);

        return is_file($this->path . $fileName);
    }

}
