<?php

declare (strict_types = 1);

namespace FileBuilder\Storage;

use Psr\Http\Message\ResponseInterface;

final class ResponseFileSystem implements StorageSystem
{

    private $response;

    /**
     * Construct
     *
     * @param $path string
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function save(string $fileName, string $fileContent): ResponseInterface
    {
        $this->response->getBody()->write($fileContent);
        $response = $this->response->withHeader('Content-Type', "text/plain");
        return $response;
    }

}
