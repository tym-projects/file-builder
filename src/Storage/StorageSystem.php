<?php

declare (strict_types = 1);

namespace FileBuilder\Storage;

interface StorageSystem
{
    public function save(string $fileName, string $fileContent);
}
