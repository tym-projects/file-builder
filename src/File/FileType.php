<?php

declare (strict_types = 1);

namespace FileBuilder\File;

interface FileType
{
    public function generateContent();
}
