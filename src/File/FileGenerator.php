<?php declare (strict_types = 1);

namespace FileBuilder\File;

use FileBuilder\File\FileDataProvider;
use FileBuilder\File\FileType;

interface FileGenerator
{
    /**
     * @param DataProvider $dataProvider
     */
    public function __construct(FileDataProvider $dataProvider, FileType $file = null);

}
