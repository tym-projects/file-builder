<?php declare (strict_types = 1);

namespace FileBuilder\File;

use ArrayIterator;
use Countable;
use FileBuilder\Exception\CollectionException;
use IteratorAggregate;

/**
 * @testFunction testCollection
 * @testFunction testRegisterCollection__construct
 */
abstract class Collection implements Countable, IteratorAggregate
{
    protected $items;

    /**
     * @param array $items
     * @return mixed
     */
    abstract public function __construct(array $items = []);

    /**
     * add new item
     * override item if $key exist
     * @param mixed $item
     * @param int|null $key
     */
    abstract public function add(mixed $item, int $key = null);

    /**
     * @param $item
     * @return void
     * @throws CollectionException
     */
    abstract protected function ValidateItem($item);

    /**
     * get item by array key
     * @param $key
     * @return null|mixed
     */
    abstract public function get($key);

    /**
     * remove list of items by key
     * @param array $keys
     */
    public function remove(array $keys)
    {
        foreach ((array) $keys as $key) {
            unset($this->items[$key]);
        }
        return $this;
    }

    /**
     * get num of items in Entity
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * get items
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }
}
