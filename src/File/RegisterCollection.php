<?php declare (strict_types = 1);

namespace FileBuilder\File;

use FileBuilder\Exception\CollectionException;
use FileBuilder\File\Collection;
use FileBuilder\File\ICAA\Entities\EntitieInterface;

class RegisterCollection extends Collection
{

    /**
     * @param array $items
     * @return void
     * @throws CollectionException
     * @testFunction testRegisterCollection__construct
     */
    public function __construct(array $items = [])
    {
        $this->items = [];
        foreach ($items as $item) {
            if (!$item instanceof EntitieInterface) {
                throw new CollectionException(sprintf("Invalid item. Each item must implement %s", EntitieInterface::class));
            }
        }
        $this->items = $items;
    }

    /**
     * @param mixed $item
     * @param int|null $key
     * @return Collection
     * @throws CollectionException
     */
    public function add($item, int $key = null)
    {
        $this->ValidateItem($item);

        if (\is_null($key)) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
    }

    /**
     * @param EntitieInterface $item
     * @return void
     * @throws CollectionException
     */
    protected function ValidateItem($item)
    {
        if (!$item instanceof EntitieInterface) {
            throw new CollectionException(sprintf("Invalid item. Each item must implement %s", EntitieInterface::class));
        }
    }

    /**
     * @param int $key
     * @return null|EntitieInterface
     */
    public function get($key): ?EntitieInterface
    {
        return (array_key_exists($key, $this->items)) ? $this->items[$key] : null;
    }

}
