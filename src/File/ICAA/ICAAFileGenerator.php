<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA;

use FileBuilder\File\FileDataProvider;
use FileBuilder\File\FileGenerator;
use FileBuilder\File\FileType;
use FileBuilder\File\ICAA\Entities\Box;
use FileBuilder\File\ICAA\Entities\Film;
use FileBuilder\File\ICAA\Entities\Room;
use FileBuilder\File\ICAA\Entities\Session;
use FileBuilder\File\ICAA\Entities\SessionFilm;
use FileBuilder\File\ICAA\Entities\SessionSchedule;
use FileBuilder\File\ICAA\ICAAFile;
use FileBuilder\File\ICAA\ValueObject\Box\BoxCode;
use FileBuilder\File\ICAA\ValueObject\Box\BoxFileLines;
use FileBuilder\File\ICAA\ValueObject\Box\BoxFileType;
use FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes;
use FileBuilder\File\ICAA\ValueObject\Box\BoxJulianDay;
use FileBuilder\File\ICAA\ValueObject\Box\BoxSessions;
use FileBuilder\File\ICAA\ValueObject\Box\BoxSpectators;
use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorCode;
use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorName;
use FileBuilder\File\ICAA\ValueObject\Film\FilmCode;
use FileBuilder\File\ICAA\ValueObject\Film\FilmFormat;
use FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmQualification;
use FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmTitle;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Room\RoomName;
use FileBuilder\File\ICAA\ValueObject\Session\SessionDate;
use FileBuilder\File\ICAA\ValueObject\Session\SessionFilms;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIssue;
use FileBuilder\File\ICAA\ValueObject\Session\SessionNumber;
use FileBuilder\File\ICAA\ValueObject\Session\SessionScheduleDate;
use FileBuilder\File\ICAA\ValueObject\Session\SessionSpectators;
use FileBuilder\File\RegisterCollection;
use InvalidArgumentException;
use Vartroth\Utils\DataConversion\Strings\FillString;

class ICAAFileGenerator implements FileGenerator
{

    /**
     *
     * @var ICAADataProvider
     */
    private $provider;

    /**
     * @var ICAAFile
     */
    private $file;

    public function __construct(
        FileDataProvider $provider,
        FileType $file = null
    ) {
        $this->provider = $provider;
        $this->file = (is_null($file)) ? new ICAAFile(
            new RegisterCollection()
        ) : $file;
    }

    /**
     * @param array $config with the next values
     *
     * $config = [
     *  'box_code'     => The Box Code from ICAA,
     *  'file_type'    => Sending file type must be one of them 'FL', 'AT', '01', '02' ...
     *  'date_last'    => Date of the last file sent
     *  'date_current' => Date of the current file sent
     * ]
     *
     * @return ICAAFile
     */
    public function generate(array $config): ICAAFile
    {
        $this->parseRoomData();
        $this->parseSessionData();
        $this->parseSessionFilm();
        $this->parseFilmData();
        $this->parseSessionsSchedules();
        $this->file->addBox(
            $this->createBox($config)
        );

        return $this->file;
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     * @throws CollectionException
     */
    private function parseRoomData(): void
    {
        foreach ($this->provider->getRoomsData() as $data) {
            $this->file->addRoom(
                new Room(
                    RoomCode::create(
                        FillString::exec($data['code'], RoomCode::LENGTH)
                    ),
                    RoomName::create(
                        FillString::exec($data['name'], RoomName::LENGTH)
                    )
                )
            );
        }
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     * @throws CollectionException
     */
    private function parseSessionData(): void
    {
        foreach ($this->provider->getSessionsData() as $data) {
            $this->file->addSession(
                new Session(
                    RoomCode::create(
                        FillString::exec($data['code'], RoomCode::LENGTH)
                    ),
                    SessionDate::create($data['date'] . " " . $data['hour']),
                    SessionFilms::create(
                        FillString::exec($data['films'], SessionFilms::LENGTH, "0", FillString::FILL_LEFT)
                    ),
                    SessionSpectators::create(
                        FillString::exec($data['spectators'], SessionSpectators::LENGTH, "0", FillString::FILL_LEFT)
                    ),
                    SessionIncomes::create(
                        FillString::exec(number_format((float) $data['incomes'], 2, '.', ''), SessionIncomes::LENGTH, "0", FillString::FILL_LEFT)
                    ),
                    SessionIssue::create($data['issue'])
                )
            );
        }
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     * @throws CollectionException
     */
    private function parseSessionFilm(): void
    {
        foreach ($this->provider->getSessionFilmsData() as $data) {
            $this->file->addSessionFilm(
                new SessionFilm(
                    RoomCode::create(
                        FillString::exec($data['code'], RoomCode::LENGTH)
                    ),
                    SessionDate::create($data['date'] . " " . $data['hour']),
                    FilmCode::create(
                        FillString::exec($data['filmId'], FilmCode::LENGTH, '0', FillString::FILL_LEFT)
                    )
                )
            );
        }
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     * @throws CollectionException
     */
    private function parseFilmData(): void
    {
        foreach ($this->provider->getFilmsData() as $data) {
            $this->file->addFilm(
                new Film(
                    RoomCode::create(
                        FillString::exec($data['code'], RoomCode::LENGTH)
                    ),
                    FilmCode::create(
                        FillString::exec($data['filmId'], FilmCode::LENGTH, '0', FillString::FILL_LEFT)
                    ),
                    FilmQualification::create(
                        FillString::exec($data['qualification'], FilmQualification::LENGTH)
                    ),
                    FilmTitle::create(
                        FillString::exec($data['title'], FilmTitle::LENGTH)
                    ),
                    DistributorCode::create(
                        FillString::exec($data['code_distri'], DistributorCode::LENGTH)
                    ),
                    DistributorName::create(
                        FillString::exec($data['name_distri'], DistributorName::LENGTH)
                    ),
                    FilmOriginalLang::create($data['vo']),
                    FilmShownLang::create($data['language']),
                    FilmSubtitlesLang::create($data['subtitles']),
                    FilmFormat::create($data['format'])
                )
            );
        }
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     * @throws CollectionException
     */
    private function parseSessionsSchedules(): void
    {
        foreach ($this->provider->getSessionsSchedules() as $data) {
            $this->file->addSessionSchedule(
                new SessionSchedule(
                    RoomCode::create(
                        FillString::exec($data['code'], RoomCode::LENGTH)
                    ),
                    SessionScheduleDate::create($data['date']),
                    SessionNumber::create(
                        FillString::exec($data['sessions'], SessionNumber::LENGTH, '0', FillString::FILL_LEFT)
                    )
                )
            );
        }
    }

    /**
     * @param array $config
     * @return Box
     * @throws InvalidArgumentException
     */
    private function createBox(array $config): Box
    {

        return new Box(
            BoxCode::create(
                FillString::exec($config['box_code'], BoxCode::LENGTH, '0', FillString::FILL_LEFT)
            ),
            BoxFileType::create($config['file_type']),
            BoxJulianDay::create(date('z', strtotime($config['date_last']))),
            BoxJulianDay::create(
                date('z', strtotime($config['date_current']))
            ),
            BoxFileLines::create(
                FillString::exec((string) ($this->file->countLines() + 1), BoxFileLines::LENGTH, '0', FillString::FILL_LEFT)
            ),
            BoxSessions::create(
                FillString::exec((string) ($this->file->getSessionCollector()->count() + 1), BoxFileLines::LENGTH, '0', FillString::FILL_LEFT)
            ),
            BoxSpectators::create(
                FillString::exec((string) ($this->file->getSpectators()), BoxFileLines::LENGTH, '0', FillString::FILL_LEFT)
            ),
            BoxIncomes::create(
                FillString::exec((string) number_format((float) $this->file->getIncomes(), 2, '.', ''), BoxFileLines::LENGTH, '0', FillString::FILL_LEFT)
            )

        );
    }

}
