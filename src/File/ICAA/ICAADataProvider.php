<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA;

use FileBuilder\File\FileDataProvider;

/**
 * @testFunction testICAADataProvider
 */
class ICAADataProvider implements FileDataProvider
{

    /**
     *
     * $code => Codigo del ICAA de Sala
     * $name => Nombre del Recinto
     *
     * @return array
     */
    public function getRoomsData(): array
    {
        $array[] = [
            'code' => "550819",
            'name' => "Hoal que hace",
        ];
        return $array;
    }

    /**
     *
     * $code => Codigo del ICAA de Sala
     * $date => Fecha de la Session
     * $hour => Hora de la Pelicula
     * $films => Numero de eventos / pelis / etc... de la session
     * $spectators => Numero de espectadores de la session
     * $incomes => Recaudación de la session
     * $issue => Incidencia en \FileBuilder\File\ICAA\ValueObject\Session\SessionIssue
     *
     * @return array
     */
    public function getSessionsData(): array
    {
        $array[] = [
            'code' => "550819",
            'date' => "12/25/2020",
            'hour' => "20:24",
            'films' => '1',
            'spectators' => '103',
            'incomes' => "12547.90",
            'issue' => "000",
        ];
        return $array;
    }

    /**
     *
     * $code => Codigo del ICAA de Sala
     * $date => Fecha de la Session
     * $hour => Hora de la Pelicula
     * $filmId => Codigo interno de la pelicula (nuestro)
     *
     * @return array
     */
    public function getSessionFilmsData(): array
    {
        $array[] = [
            'code' => "550819",
            'date' => "12/25/2020",
            'hour' => "20:24",
            'filmId' => '254',
        ];
        return $array;
    }

    /**
     * $code => Codigo del ICAA de Sala
     * $filmId => Codigo interno de la pelicula (nuestro)
     * $qualification => Código de expediente de calificación de la película, suministrado por el ICAA.
     * $title => Título de la película
     * $code_distri => Codigo Distribuidora
     * $name_distri => Nombre Distribuidora
     * $vo => Version original de la pelicula \FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang
     * $language => Idioma de Proyección \FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang
     * $subtitles => Subtitulos de la película \FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang
     * $format => Formato de proyección \FileBuilder\File\ICAA\ValueObject\Film\FilmFormat
     *
     * @return array
     */
    public function getFilmsData(): array
    {
        $array[] = [
            'code' => "550819",
            'filmId' => '254',
            'qualification' => "25466",
            'title' => "Los mongolos de Scorpia",
            'code_distri' => '251',
            'name_distri' => "Paco Producciones SL",
            'vo' => "6",
            'language' => "W",
            'subtitles' => "0",
            'format' => "1",

        ];
        return $array;
    }

    /**
     * $code => Codigo del ICAA de Sala
     * $date => Fecha de la Session
     * $session => Numero de sessiones en la sala ese dia
     *
     * @return array
     */
    public function getSessionsSchedules(): array
    {
        $array[] = [
            'code' => "550819",
            'date' => "12/25/2020",
            'sessions' => '3',
        ];
        return $array;
    }

    /**
     * @param string $initial_date
     * @param string $final_date
     * @return array
     */
    public function getRoomIncidents(
        string $initial_date,
        string $final_date
    ): array{

        $array[] = [
            'code' => "550819",
            'monday' => true,
            'tuesday' => true,
            'wednesday' => true,
            'thursday' => true,
            'friday' => false,
            'saturday' => false,
            'sunday' => false,
            'comment' => "No hay programación",
        ];

        return $array;
    }

}
