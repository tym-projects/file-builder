<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA;

use FileBuilder\File\FileDataProvider;
use FileBuilder\File\FileGenerator;
use FileBuilder\File\FileType;
use FileBuilder\File\ICAA\Entities\HeaderIncident;
use FileBuilder\File\ICAA\Entities\RoomIncident;
use FileBuilder\File\ICAA\ICAAIncidentFile;
use FileBuilder\File\ICAA\ValueObject\Incident\BoxCode;
use FileBuilder\File\ICAA\ValueObject\Incident\HasIssue;
use FileBuilder\File\ICAA\ValueObject\Incident\IncidentComment;
use FileBuilder\File\ICAA\ValueObject\Incident\IncidentDate;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\RegisterCollection;
use InvalidArgumentException;
use Vartroth\Utils\DataConversion\Strings\FillString;

class ICAAFileIncidentGenerator implements FileGenerator
{

    /**
     *
     * @var ICAADataProvider
     */
    private $provider;

    /**
     * @var ICAAIncidentFile
     */
    private $file;

    public function __construct(
        FileDataProvider $provider,
        FileType $file = null
    ) {
        $this->provider = $provider;
        $this->file = (is_null($file)) ? new ICAAIncidentFile(
            new RegisterCollection()
        ) : $file;
    }

    /**
     * @param array $config with the next values
     *
     * $config = [
     *  'box_code'     => The Box Code from ICAA,
     *  'initial_date' => Date of initial day of the week
     *  'final_date'   => Date of final day of the week
     * ]
     *
     * @return ICAAIncidentFile
     */
    public function generate(array $config): ICAAIncidentFile
    {
        $this->parseHeader($config);
        $this->parseRoomIncidents($config);

        return $this->file;
    }

    /**
     * @param array $config
     * @return ICAAIncidentFile
     * @throws InvalidArgumentException
     */
    private function parseHeader(array $config): ICAAIncidentFile
    {
        $this->file->addHeader(
            new HeaderIncident(
                BoxCode::create(
                    FillString::exec($config['box_code'], BoxCode::LENGTH, '0', FillString::FILL_LEFT)
                ),
                IncidentDate::create($config['initial_date']),
                IncidentDate::create($config['final_date'])
            )
        );

        return $this->file;
    }

    /**
     * @param array $config
     * @return ICAAIncidentFile
     * @throws InvalidArgumentException
     * @throws CollectionException
     */
    private function parseRoomIncidents(array $config): ICAAIncidentFile
    {

        foreach ($this->provider->getRoomIncidents($config['initial_date'], $config['final_date']) as $data) {
            $this->file->addRoom(
                new RoomIncident(
                    RoomCode::create(
                        FillString::exec($data['code'], RoomCode::LENGTH)
                    ),
                    HasIssue::create((bool) ($data['monday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    HasIssue::create((bool) ($data['tuesday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    HasIssue::create((bool) ($data['wednesday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    HasIssue::create((bool) ($data['thursday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    HasIssue::create((bool) ($data['friday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    HasIssue::create((bool) ($data['saturday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    HasIssue::create((bool) ($data['sunday']) ? HasIssue::HAS_ISSUE : HasIssue::NO_ISSUE),
                    IncidentComment::create(
                        FillString::exec($data['comment'], IncidentComment::MAX_LENGTH)
                    )
                )
            );
        }
        return $this->file;
    }

}
