<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Incident\BoxCode;
use FileBuilder\File\ICAA\ValueObject\Incident\IncidentDate;

/**
 * BoxIncident class
 *
 * @testFunction testHeaderIncident
 */
class HeaderIncident implements EntitieInterface
{
    const REGISTER_TYPE = 0;
    const FILE_TYPE = "IN";
    const LINE_LENGHT = 16;

    /**
     * @var BoxCode
     */
    private $boxCode;

    /**
     * @var IncidentDate
     */
    private $initialIncidentDate;

    /**
     * @var IncidentDate
     */
    private $finalIncidentDate;

    /**
     * @param BoxCode $boxCode
     * @param IncidentDate $initialIncidentDate
     * @param IncidentDate $finalIncidentDate
     */
    public function __construct(
        BoxCode $boxCode,
        IncidentDate $initialIncidentDate,
        IncidentDate $finalIncidentDate
    ) {

        $this->boxCode = $boxCode;
        $this->initialIncidentDate = $initialIncidentDate;
        $this->finalIncidentDate = $finalIncidentDate;

    }

    /**
     * @return string
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->boxCode->__toString() .
        $this->initialIncidentDate->__toString() .
        $this->finalIncidentDate->__toString();
    }

    /**
     * Get the value of boxCode
     *
     * @return  BoxCode
     */
    public function getBoxCode(): BoxCode
    {
        return $this->boxCode;
    }

    /**
     * Get the value of initialIncidentDate
     *
     * @return  IncidentDate
     */
    public function getInitialIncidentDate(): IncidentDate
    {
        return $this->initialIncidentDate;
    }
}
