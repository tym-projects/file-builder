<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Box\BoxCode;
use FileBuilder\File\ICAA\ValueObject\Box\BoxFileLines;
use FileBuilder\File\ICAA\ValueObject\Box\BoxFileType;
use FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes;
use FileBuilder\File\ICAA\ValueObject\Box\BoxJulianDay;
use FileBuilder\File\ICAA\ValueObject\Box\BoxSessions;
use FileBuilder\File\ICAA\ValueObject\Box\BoxSpectators;

/**
 * Box class
 *
 * With general information about the mailbox that sends the file
 * 56 characters long
 * There will be a single type 0 record for each file sent
 *
 */
class Box implements EntitieInterface
{
    const REGISTER_TYPE = 0;
    const LINE_LENGHT = 56;

    /**
     * @var BoxCode
     */
    private $boxCode;

    /**
     * @var BoxFileType
     */
    private $type;

    /**
     * @var BoxJulianDay
     */
    private $lastFileSentAt;

    /**
     * @var BoxJulianDay
     */
    private $currentFileSentAt;

    /**
     * @var BoxFileLines
     */
    private $fileLines;

    /**
     * @var BoxSessions
     */
    private $sessions;

    /**
     * @var BoxSpectators
     */
    private $spectators;

    /**
     * @var BoxIncomes
     */
    private $incomes;

    /**
     * @testFunction testBoxInLine
     * @testFunction testBox__constructInvalidValueException
     */
    public function __construct(
        BoxCode $boxCode,
        BoxFileType $type,
        BoxJulianDay $lastFileSentAt,
        BoxJulianDay $currentFileSentAt,
        BoxFileLines $fileLines,
        BoxSessions $sessions,
        BoxSpectators $spectators,
        BoxIncomes $incomes

    ) {

        $this->boxCode = $boxCode;
        $this->type = $type;
        $this->lastFileSentAt = $lastFileSentAt;
        $this->currentFileSentAt = $currentFileSentAt;
        $this->fileLines = $fileLines;
        $this->sessions = $sessions;
        $this->spectators = $spectators;
        $this->incomes = $incomes;

    }

    /**
     * valuesInLine function
     *
     * @return string
     * @testFunction testBoxInLine
     */
    public function inLine(): string
    {

        return self::REGISTER_TYPE .
        $this->boxCode->__toString() .
        $this->type->__toString() .
        $this->lastFileSentAt->__toString() .
        $this->currentFileSentAt->__toString() .
        $this->fileLines->__toString() .
        $this->sessions->__toString() .
        $this->spectators->__toString() .
        $this->incomes->__toString();

    }
}
