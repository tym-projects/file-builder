<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorCode;
use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorName;
use FileBuilder\File\ICAA\ValueObject\Film\FilmCode;
use FileBuilder\File\ICAA\ValueObject\Film\FilmFormat;
use FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmQualification;
use FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmTitle;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;

/**
 * @testFunction testFilm
 */
class Film implements EntitieInterface
{

    const REGISTER_TYPE = 4;
    const LINE_LENGHT = 146;

    /**
     * @var RoomCode
     */
    private $code;

    /**
     * @var FilmCode
     */
    private $filmCode;

    /**
     * @var FilmQualification
     */
    private $filmQualification;

    /**
     * @var FilmTitle
     */
    private $filmTitle;

    /**
     * @var DistributorCode
     */
    private $distributorCode;

    /**
     * @var DistributorName
     */
    private $distributorName;

    /**
     * @var FilmOriginalLang
     */
    private $originalLang;

    /**
     * @var FilmShownLang
     */
    private $shownLang;

    /**
     * @var FilmSubtitlesLang
     */
    private $subtitles;

    /**
     * @var FilmFormat
     */
    private $filmFormat;

    public function __construct(
        RoomCode $code,
        FilmCode $filmCode,
        FilmQualification $filmQualification,
        FilmTitle $filmTitle,
        DistributorCode $distributorCode,
        DistributorName $distributorName,
        FilmOriginalLang $originalLang,
        FilmShownLang $shownLang,
        FilmSubtitlesLang $subtitles,
        FilmFormat $filmFormat
    ) {
        $this->code = $code;
        $this->filmCode = $filmCode;
        $this->filmQualification = $filmQualification;
        $this->filmTitle = $filmTitle;
        $this->distributorCode = $distributorCode;
        $this->distributorName = $distributorName;
        $this->originalLang = $originalLang;
        $this->shownLang = $shownLang;
        $this->subtitles = $subtitles;
        $this->filmFormat = $filmFormat;

    }

    /**
     * inLine function
     *
     * @return string
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->code->__toString() .
        $this->filmCode->__toString() .
        $this->filmQualification->__toString() .
        $this->filmTitle->__toString() .
        $this->distributorCode->__toString() .
        $this->distributorName->__toString() .
        $this->originalLang->__toString() .
        $this->shownLang->__toString() .
        $this->subtitles->__toString() .
        $this->filmFormat->__toString();
    }

}
