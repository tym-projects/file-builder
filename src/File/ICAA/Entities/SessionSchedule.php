<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Session\SessionNumber;
use FileBuilder\File\ICAA\ValueObject\Session\SessionScheduleDate;

class SessionSchedule implements EntitieInterface
{

    const REGISTER_TYPE = 5;
    const LINE_LENGHT = 21;

    /**
     * @var RoomCode
     */
    private $code;

    /**
     * @var SessionScheduleDate
     */
    private $date;

    /**
     * @var SessionNumber
     */
    private $sessionNumber;

    public function __construct(
        RoomCode $code,
        SessionScheduleDate $date,
        SessionNumber $sessionNumber
    ) {
        $this->code = $code;
        $this->date = $date;
        $this->sessionNumber = $sessionNumber;

    }
    /**
     * inLine function
     *
     * @return string
     * @testFunction testSessionScheduleInLine
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->code->__toString() .
        $this->date->__toString() .
        $this->sessionNumber->__toString();
    }
}
