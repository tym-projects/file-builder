<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Film\FilmCode;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Session\SessionDate;

/**
 * @testFunction testSessionFilm
 */
class SessionFilm implements EntitieInterface
{

    const REGISTER_TYPE = 3;
    const LINE_LENGHT = 28;

    /**
     * @var RoomCode
     */
    private $code;

    /**
     * @var SessionDate
     */
    private $date;

    /**
     * @var FilmCode
     */
    private $filmCode;

    public function __construct(
        RoomCode $code,
        SessionDate $date,
        FilmCode $filmCode
    ) {
        $this->code = $code;
        $this->date = $date;
        $this->filmCode = $filmCode;

    }
    /**
     * inLine function
     *
     * @return string
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->code->__toString() .
        $this->date->__toString() .
        $this->filmCode->__toString();
    }

}
