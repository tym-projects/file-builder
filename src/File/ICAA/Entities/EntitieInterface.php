<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

interface EntitieInterface
{

    /**
     * inLine function
     * @return string
     */
    public function inLine(): string;

}
