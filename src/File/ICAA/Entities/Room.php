<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Room\RoomName;

class Room implements EntitieInterface
{
    const REGISTER_TYPE = 1;
    const LINE_LENGHT = 43;

    /**
     * @var RoomCode
     */
    private $code;

    /**
     * @var RoomName
     */
    private $name;

    /**
     * CinemaTheatre constructor.
     * @param string $code
     * @param string $name
     * @testFunction testRoomInLine
     */
    public function __construct(RoomCode $code, RoomName $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * valuesInLine function
     * @return string
     * @testFunction testRoomInLine
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->code->__toString() .
        $this->name->__toString();
    }
}
