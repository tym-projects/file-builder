<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Incident\HasIssue;
use FileBuilder\File\ICAA\ValueObject\Incident\IncidentComment;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;

/**
 * @testFunction testRoomIncident
 */
class RoomIncident implements EntitieInterface
{
    const REGISTER_TYPE = 1;
    const LINE_LENGHT = 100;

    /**
     * @var RoomCode
     */
    private $code;

    /**
     * @var HasIssue
     */
    private $monday;

    /**
     * @var HasIssue
     */
    private $tuesday;

    /**
     * @var HasIssue
     */
    private $wednesday;

    /**
     * @var HasIssue
     */
    private $thursday;

    /**
     * @var HasIssue
     */
    private $friday;

    /**
     * @var HasIssue
     */
    private $saturday;

    /**
     * @var HasIssue
     */
    private $sunday;

    private $comment;

    /**
     * @param RoomCode $code
     * @param HasIssue $monday
     * @param HasIssue $tuesday
     * @param HasIssue $wednesday
     * @param HasIssue $thursday
     * @param HasIssue $friday
     * @param HasIssue $saturday
     * @param HasIssue $sunday
     * @param IncidentComment $comment
     * @return void
     */
    public function __construct(
        RoomCode $code,
        HasIssue $monday,
        HasIssue $tuesday,
        HasIssue $wednesday,
        HasIssue $thursday,
        HasIssue $friday,
        HasIssue $saturday,
        HasIssue $sunday,
        IncidentComment $comment
    ) {
        $this->code = $code;
        $this->monday = $monday;
        $this->tuesday = $tuesday;
        $this->wednesday = $wednesday;
        $this->thursday = $thursday;
        $this->friday = $friday;
        $this->saturday = $saturday;
        $this->sunday = $sunday;
        $this->comment = $comment;
    }

    /**
     * valuesInLine function
     * @return string
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->code->__toString() .
        $this->monday->__toString() .
        $this->tuesday->__toString() .
        $this->wednesday->__toString() .
        $this->thursday->__toString() .
        $this->friday->__toString() .
        $this->saturday->__toString() .
        $this->sunday->__toString() .
        $this->comment->__toString();
    }
}
