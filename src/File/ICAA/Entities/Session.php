<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Session\SessionDate;
use FileBuilder\File\ICAA\ValueObject\Session\SessionFilms;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIssue;
use FileBuilder\File\ICAA\ValueObject\Session\SessionSpectators;

/**
 * @testFunction testSession
 */
class Session implements EntitieInterface
{
    const REGISTER_TYPE = 2;
    const LINE_LENGHT = 41;

    /**
     * @var RoomCode
     */
    private $code;

    /**
     * @var SessionDate
     */
    private $date;

    /**
     *
     * @var SessionFilms
     */
    private $films;

    /**
     * @var SessionSpectators
     */
    private $spectators;

    /**
     * @var SessionIncomes
     */
    private $incomes;

    /**
     * @var SessionIssue
     */
    private $issue;

    public function __construct(
        RoomCode $code,
        SessionDate $date,
        SessionFilms $films,
        SessionSpectators $spectators,
        SessionIncomes $incomes,
        SessionIssue $issue

    ) {
        $this->code = $code;
        $this->date = $date;
        $this->films = $films;
        $this->spectators = $spectators;
        $this->incomes = $incomes;
        $this->issue = $issue;
    }

    /**
     * valuesInLine function
     * @return string
     */
    public function inLine(): string
    {
        return self::REGISTER_TYPE .
        $this->code->__toString() .
        $this->date->__toString() .
        $this->films->__toString() .
        $this->spectators->__toString() .
        $this->incomes->__toString() .
        $this->issue->__toString();
    }

    /**
     * Get the value of spectators
     *
     * @return  SessionSpectators
     */
    public function getSpectators(): string
    {
        return $this->spectators->__toString();
    }

    /**
     * Get the value of incomes
     *
     * @return  SessionIncomes
     */
    public function getIncomes(): string
    {
        return $this->incomes->__toString();
    }
}
