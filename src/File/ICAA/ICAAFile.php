<?php

declare (strict_types = 1);

namespace FileBuilder\File\ICAA;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\FileType;
use FileBuilder\File\ICAA\Entities\Box;
use FileBuilder\File\ICAA\Entities\Film;
use FileBuilder\File\ICAA\Entities\Room;
use FileBuilder\File\ICAA\Entities\Session;
use FileBuilder\File\ICAA\Entities\SessionFilm;
use FileBuilder\File\ICAA\Entities\SessionSchedule;
use FileBuilder\File\RegisterCollection;

/**
 * @testFunction testICAAFile
 */
class ICAAFile implements FileType
{

    /**
     * Default File Code
     */
    const USUAL_FILE = 'FL';

    /**
     * Overdue File Code
     */
    const OVERDUE_FILE = 'AT';

    /**
     * End Of Line Characters
     */
    const EOL = PHP_EOL;

    /**
     * Valid File Types
     */
    const FILE_TYPE_VALUES = [
        self::USUAL_FILE,
        self::OVERDUE_FILE,
        "01",
        "02",
        "03",
        "04",
    ];

    /**
     * @var null|Box
     */
    private $box;

    /**
     *
     * @var RegisterCollection
     */
    private $roomCollection;

    /**
     * @var RegisterCollection
     */
    private $sessionCollector;

    /**
     * @var RegisterCollection
     */
    private $sessionFilmCollector;

    /**
     * @var RegisterCollection
     */
    private $filmCollector;

    /**
     * @var RegisterCollection
     */
    private $sessionScheduleCollector;

    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $spectators;

    /**
     * @var float
     */
    private $incomes;

    /**
     * @return void
     */
    public function __construct()
    {

        $this->box = null;
        $this->roomCollection = new RegisterCollection();
        $this->sessionCollector = new RegisterCollection();
        $this->sessionFilmCollector = new RegisterCollection();
        $this->filmCollector = new RegisterCollection();
        $this->sessionScheduleCollector = new RegisterCollection();
        $this->content = "";
    }

    /**
     * @param Box $box
     * @return void
     */
    public function addBox(Box $box)
    {
        $this->box = $box;
    }

    /**
     * @param Room $room
     * @throws CollectionException
     */
    public function addRoom(Room $room)
    {
        $this->roomCollection->add($room);
    }

    /**
     * @param Session $session
     * @throws CollectionException
     */
    public function addSession(Session $session)
    {
        $this->spectators += (int) $session->getSpectators();
        $this->incomes += (float) $session->getIncomes();

        $this->sessionCollector->add($session);
    }

    /**
     * @param SessionFilm $sessionFilm
     * @throws CollectionException
     */
    public function addSessionFilm(SessionFilm $sessionFilm)
    {
        $this->sessionFilmCollector->add($sessionFilm);
    }

    /**
     * @param SessionSchedule $sessionSchedule
     * @throws CollectionException
     */
    public function addSessionSchedule(SessionSchedule $sessionSchedule)
    {
        $this->sessionScheduleCollector->add($sessionSchedule);
    }

    /**
     * @param Film $film
     * @throws CollectionException
     */
    public function addFilm(Film $film)
    {
        $this->filmCollector->add($film);
    }

    /**
     * @return ICAAFile
     * @throws ValueException
     */
    public function generateContent(): string
    {
        if (is_null($this->box)) {
            throw new ValueException("The BoxLine is null, you must before parse data into the box line");
        }

        $this->content = $this->box->inLine() . self::EOL;
        $this->content .= $this->getRegisterCollectionLines($this->roomCollection);
        $this->content .= $this->getRegisterCollectionLines($this->sessionCollector);
        $this->content .= $this->getRegisterCollectionLines($this->sessionFilmCollector);
        $this->content .= $this->getRegisterCollectionLines($this->filmCollector);
        $this->content .= $this->getRegisterCollectionLines($this->sessionScheduleCollector);

        return $this->content;
    }

    /**
     * This function returns number of lines in file without the box line
     * @return int
     */
    public function countLines(): int
    {
        return (
            $this->roomCollection->count() +
            $this->sessionCollector->count() +
            $this->sessionFilmCollector->count() +
            $this->filmCollector->count() +
            $this->sessionScheduleCollector->count()
        );
    }

    /**
     * @param RegisterCollection $collection
     * @return string
     */
    private function getRegisterCollectionLines(RegisterCollection $collection): string
    {
        $str = "";
        $it = $collection->getIterator();
        for ($it = $collection->getIterator(); $it->valid(); $it->next()) {
            $str .= $it->current()->inLine() . self::EOL;
        }
        return $str;
    }

    /**
     * Get the value of spectators
     *
     * @return  int
     */
    public function getSpectators(): int
    {
        return $this->spectators;
    }

    /**
     * Get the value of incomes
     *
     * @return  float
     */
    public function getIncomes(): float
    {
        return $this->incomes;
    }

    /**
     * Get the value of sessionCollector
     *
     * @return  RegisterCollection
     */
    public function getSessionCollector()
    {
        return $this->sessionCollector;
    }
}
