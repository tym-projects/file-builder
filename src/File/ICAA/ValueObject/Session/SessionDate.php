<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Session;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testSessionDate
 */
class SessionDate implements Stringable
{
    const LENGTH = 10;
    const DATE_FORMAT = "dmyHi";

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionDate function
     *
     * @param string $value
     * @return SessionDate
     * @throws InvalidArgumentException
     */
    public static function create(string $value): SessionDate
    {
        if (!strtotime($value)) {
            throw new InvalidArgumentException(sprintf('The value "%s" is not a valid date', $value, "SessionDate"));
        }

        $value_formatted = date(self::DATE_FORMAT, strtotime($value));

        return new SessionDate($value_formatted);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
