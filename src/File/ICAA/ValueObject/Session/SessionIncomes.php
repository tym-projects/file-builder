<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Session;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testSessionIncomes
 */
class SessionIncomes implements Stringable
{

    const LENGTH = 8;
    const DOT_POSTITION = 6;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionIncomes function
     *
     * @param string $value
     * @return SessionIncomes
     * @throws InvalidArgumentException
     */
    public static function create(string $value): SessionIncomes
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "SessionIncomes"));
        }

        if (strpos($value, '.') != self::DOT_POSTITION - 1) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong dot position', $value, "SessionIncomes"));
        }

        return new SessionIncomes($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
