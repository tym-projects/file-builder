<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Session;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testSessionSpectators
 */
class SessionSpectators implements Stringable
{
    const LENGTH = 5;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionSpectators function
     *
     * @param string $value
     * @return SessionSpectators
     * @throws InvalidArgumentException
     */
    public static function create(string $value): SessionSpectators
    {

        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(
                sprintf('The value "%s" in %s has the wrong length', $value, "SessionSpectators")
            );
        }

        return new SessionSpectators($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
