<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\Exception\ValueException;
use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testSessionIssue
 */
class SessionIssue implements Stringable
{

    /**
     * No Issues value
     */
    const NO_ISSUE = "000";

    /**
     * Manual Sale (downgraded)
     * The computer system is down and sold manually.
     * Later it is introduced in the application.
     */
    const MS = "005";

    /**
     * Manual Sale + Sales Cancellation
     */
    const MS_CANCEL = "006";

    /**
     * Manual sale of current session + Cancellation of Programming (previous session at the same time).
     * Manual sale of current session + Cancellation of Previous programming at the same time due to programming failure.
     */
    const MS_CANCEL_PROGRAMING = "007";

    /**
     * Manual sale current session + Cancellation of sales current session + Cancellation of Schedule (previous session)
     */
    const MS_CANCELL_SALES_CURRENT_CPREV = "008";

    /**
     * Cancellation of sales
     * There is an cancellation of sales, regardless of whether or not those seats are sold later.
     */
    const CANCELLATION_SALES = "009";

    /**
     * Programming Cancellation (previous session at the same time)
     * All programming is canceled and another session is started at the same time due to programming failure
     */
    const PROGRAMMING_CANCEL_START_OTHER = "010";

    /**
     * Sales cancellation current session + Programming cancellation (previous session at the same time)
     */
    const PROGRAMMING_CANCELLATION = "011";

    /**
     * List of valid codes for issues in session
     */
    const VALID_CODES = [
        self::NO_ISSUE,
        self::MS,
        self::MS_CANCEL,
        self::MS_CANCEL_PROGRAMING,
        self::MS_CANCELL_SALES_CURRENT_CPREV,
        self::CANCELLATION_SALES,
        self::PROGRAMMING_CANCEL_START_OTHER,
        self::PROGRAMMING_CANCELLATION,
    ];

    const LENGTH = 3;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionIssue function
     *
     * @param string $value
     * @return SessionIssue
     * @throws InvalidArgumentException
     */
    public static function create(string $value = self::NO_ISSUE): SessionIssue
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "SessionIssue"));
        }

        if (!in_array($value, self::VALID_CODES)) {
            throw new ValueException(sprintf('The value "%s" in %s is not valid', $value, "SessionIssue"));
        }

        return new SessionIssue($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
