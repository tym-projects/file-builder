<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Session;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testSessionNumber
 */
class SessionNumber implements Stringable
{

    const LENGTH = 2;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionNumber function
     *
     * @param string $value
     * @return SessionNumber
     * @throws InvalidArgumentException
     */
    /**
     * @testFunction testSessionNumberCreate
     */
    public static function create(string $value): SessionNumber
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "SessionNumber"));
        }

        return new SessionNumber($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
