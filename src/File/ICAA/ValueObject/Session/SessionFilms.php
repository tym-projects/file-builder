<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Session;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testSessionFilms
 */
class SessionFilms implements Stringable
{
    const LENGTH = 2;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionFilms function
     *
     * @param string $value
     * @return SessionFilms
     * @throws InvalidArgumentException
     */
    public static function create(string $value): SessionFilms
    {

        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(
                sprintf('The value "%s" in %s has the wrong length', $value, "SessionFilms")
            );
        }

        return new SessionFilms($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
