<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Room;

use InvalidArgumentException;
use Stringable;

/**
 * RoomName class
 *
 * Room name
 *
 */
class RoomName implements Stringable
{

    const LENGTH = 30;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length RoomName function
     *
     * @param string $value
     * @return RoomName
     * @throws InvalidArgumentException
     */
    /**
     * @testFunction testRoomName
     */
    public static function create(string $value): RoomName
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "RoomName"));
        }

        return new RoomName($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
