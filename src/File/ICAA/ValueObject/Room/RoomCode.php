<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Room;

use InvalidArgumentException;
use Stringable;

/**
 * RoomCode class
 *
 * Room code. six-digit code supplied by the ICAA
 * Next digits must be spaces
 *
 * @testFunction testRoomCode
 */
class RoomCode implements Stringable
{

    const LENGTH = 12;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length RoomCode function
     *
     * @param string $value
     * @return
     * @throws InvalidArgumentException
     */
    public static function create(string $value): RoomCode
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "RoomCode"));
        }

        return new RoomCode($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
