<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Incident;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testIncidentDate
 */
class IncidentDate implements Stringable
{
    const LENGTH = 6;
    const DATE_FORMAT = "dmy";

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length SessionScheduleDate function
     *
     * @param string $value
     * @return SessionScheduleDate
     * @throws InvalidArgumentException
     */
    public static function create(string $value): IncidentDate
    {
        if (!strtotime($value)) {
            throw new InvalidArgumentException(sprintf('The value "%s" is not a valid date', $value, "IncidentDate"));
        }

        $value_formatted = date(self::DATE_FORMAT, strtotime($value));

        return new self($value_formatted);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
