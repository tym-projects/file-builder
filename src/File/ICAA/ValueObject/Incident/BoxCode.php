<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Incident;

use InvalidArgumentException;
use Stringable;

/**
 * BoxCode class
 *
 * Mailbox code. Three-digit code supplied by the ICAA
 *
 * @testFunction testBoxCode
 */
class BoxCode implements Stringable
{

    const LENGTH = 3;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length BoxCode function
     *
     * @param string $value
     * @return BoxCode
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxCode
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxCode"));
        }

        return new BoxCode($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
