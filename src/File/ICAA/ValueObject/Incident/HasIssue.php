<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Incident;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testHasIssue
 */
class HasIssue implements Stringable
{
    const HAS_ISSUE = "0";
    const NO_ISSUE = "1";

    const LENGTH = 1;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length HasIssue function
     *
     * @param string $value default NO_ISSUE
     * @return HasIssue
     * @throws InvalidArgumentException
     */
    public static function create(string $value = self::NO_ISSUE): HasIssue
    {
        if ($value != self::HAS_ISSUE && $value != self::NO_ISSUE) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s are not valid', $value, "HasIssue"));
        }

        return new HasIssue($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
