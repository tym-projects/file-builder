<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Incident;

use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testIncidentComment
 */
class IncidentComment implements Stringable
{

    const NO_VALUE = "";
    const MAX_LENGTH = 80;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length IncidentComment function
     *
     * @param string $value default value NO_VALUE
     * @return IncidentComment
     * @throws InvalidArgumentException
     */
    public static function create(string $value = self::NO_VALUE): IncidentComment
    {
        if (mb_strlen($value) > self::MAX_LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "IncidentComment"));
        }

        return new IncidentComment($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
