<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testFilmOriginalLang
 */
class FilmOriginalLang implements Stringable
{
    const SPANISH = "1";
    const CATALAN = "2";
    const VASCO = "3";
    const GALLEGO = "4";
    const VALENCIANO = "5";
    const ENGLISH = "6";
    const FRENCH = "7";
    const GERMAN = "8";
    const PORTUGUESE = "9";
    const ITALIAN = "A";
    const OTHER_LANGUAGES = "Z";

    /**
     * List of valid codes for film original languages
     */
    const VALID_CODES = [
        self::SPANISH,
        self::CATALAN,
        self::VASCO,
        self::GALLEGO,
        self::VALENCIANO,
        self::ENGLISH,
        self::FRENCH,
        self::GERMAN,
        self::ITALIAN,
        self::OTHER_LANGUAGES,
    ];

    const LENGTH = 1;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmOriginalLang function
     *
     * @param string $value
     * @return FilmOriginalLang
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmOriginalLang
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmOriginalLang"));
        }

        if (!in_array($value, self::VALID_CODES)) {
            throw new ValueException(sprintf('The value "%s" in %s is not valid', $value, "FilmOriginalLang"));
        }

        return new FilmOriginalLang($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
