<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testFilmShownLang
 */
class FilmShownLang implements Stringable
{
    const ORIGINAL_SPANISH = "S";
    const ORIGINAL_VALENCIANO = "B";
    const ORIGINAL_CATALAN = "R";
    const ORIGINAL_VASCO = "Q";
    const ORIGINAL_GALLEGO = "P";
    const ORIGINAL_OTHER_LANGUAGES = "Z";
    const DUBBING_SPANISH = "W";
    const DUBBING_VALENCIANO = "Y";
    const DUBBING_CATALAN = "V";
    const DUBBING_VASCO = "U";
    const DUBBING_GALLEGO = "T";
    const DUBBING_OTHER_LANGUAGES = "F";
    const NO_DIALOGUE = "X";

    /**
     * List of valid codes for film languages
     */
    const VALID_CODES = [
        self::ORIGINAL_SPANISH,
        self::ORIGINAL_VALENCIANO,
        self::ORIGINAL_CATALAN,
        self::ORIGINAL_VASCO,
        self::ORIGINAL_GALLEGO,
        self::ORIGINAL_OTHER_LANGUAGES,
        self::DUBBING_SPANISH,
        self::DUBBING_VALENCIANO,
        self::DUBBING_CATALAN,
        self::DUBBING_VASCO,
        self::DUBBING_GALLEGO,
        self::DUBBING_OTHER_LANGUAGES,
        self::NO_DIALOGUE,
    ];

    const LENGTH = 1;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmShownLang function
     *
     * @param string $value
     * @return FilmShownLang
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmShownLang
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmShownLang"));
        }

        if (!in_array($value, self::VALID_CODES)) {
            throw new ValueException(sprintf('The value "%s" in %s is not valid', $value, "FilmShownLang"));
        }

        return new FilmShownLang($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
