<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testFilmFormat
 */
class FilmFormat implements Stringable
{
    const F_35MM = "0";
    const DIGITAL2D = "1";
    const DIGITAL3D = "2";
    const DVD = "3";
    const BLURAY = "4";
    const BETACAM = "5";
    const VHS = "6";
    const IMAX = "7";
    const OMNIMAX = "8";
    const BETAMAX = "9";

    /**
     * List of valid codes for film original languages
     */
    const VALID_CODES = [
        self::F_35MM,
        self::DIGITAL2D,
        self::DIGITAL3D,
        self::DVD,
        self::BLURAY,
        self::BETACAM,
        self::VHS,
        self::IMAX,
        self::OMNIMAX,
        self::BETAMAX,
    ];

    const LENGTH = 1;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmFormat function
     *
     * @param string $value
     * @return FilmFormat
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmFormat
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmFormat"));
        }

        if (!in_array($value, self::VALID_CODES)) {
            throw new ValueException(sprintf('The value "%s" in %s is not valid', $value, "FilmFormat"));
        }

        return new FilmFormat($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
