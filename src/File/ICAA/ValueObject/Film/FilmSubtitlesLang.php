<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use InvalidArgumentException;
use Stringable;

/**
 * @testFunction testFilmSubtitlesLang
 */
class FilmSubtitlesLang implements Stringable
{
    const SPANISH = "1";
    const CATALAN = "2";
    const VASCO = "3";
    const GALLEGO = "4";
    const VALENCIANO = "5";
    const OTHER_LANGUAGES = "9";
    const NO_SUBTITLES = "0";

    /**
     * List of valid subtitles codes for film language
     */
    const VALID_CODES = [
        self::SPANISH,
        self::CATALAN,
        self::VASCO,
        self::GALLEGO,
        self::VALENCIANO,
        self::OTHER_LANGUAGES,
        self::NO_SUBTITLES,
    ];

    const LENGTH = 1;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmSubtitlesLang function
     *
     * @param string $value
     * @return FilmSubtitlesLang
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmSubtitlesLang
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmSubtitlesLang"));
        }

        if (!in_array($value, self::VALID_CODES)) {
            throw new ValueException(sprintf('The value "%s" in %s is not valid', $value, "FilmSubtitlesLang"));
        }

        return new FilmSubtitlesLang($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
