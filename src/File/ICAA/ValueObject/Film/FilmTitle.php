<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use InvalidArgumentException;
use Stringable;

/**
 * FilmTitle class
 *
 * @testFunction testFilmTitle
 * @testFunction testFilmTitle
 */
class FilmTitle implements Stringable
{

    const LENGTH = 50;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmTitle function
     *
     * @param string $value
     * @return FilmTitle
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmTitle
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmTitle"));
        }

        return new FilmTitle($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
