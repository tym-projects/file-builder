<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use InvalidArgumentException;
use Stringable;

/**
 * FilmCode class
 *
 * @testFunction testFilmCode
 * @testFunction testBoxCodeCreateInvalidValueException
 *
 */
class FilmCode implements Stringable
{

    const LENGTH = 5;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmCode function
     *
     * @param string $value
     * @return FilmCode
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmCode
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmCode"));
        }

        return new FilmCode($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
