<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Film;

use InvalidArgumentException;
use Stringable;

/**
 * FilmQualification class
 *
 * @testFunction testFilmQualification
 */
class FilmQualification implements Stringable
{

    const LENGTH = 12;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length FilmQualification function
     *
     * @param string $value
     * @return FilmQualification
     * @throws InvalidArgumentException
     */
    public static function create(string $value): FilmQualification
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "FilmQualification"));
        }

        return new FilmQualification($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
