<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Box;

use InvalidArgumentException;
use Stringable;
use Vartroth\Utils\DataConversion\Strings\FillString;

/**
 * BoxJulianDay class
 *
 * Julian number of the day
 * Three characters, padding with leading zeros if necessary
 *
 */
class BoxJulianDay implements Stringable
{

    const LENGTH = 3;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length BoxJulianDay function
     *
     * @param string $value
     * @return BoxJulianDay
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxJulianDay
    {
        $value = FillString::exec($value, self::LENGTH, "0", FillString::FILL_LEFT);

        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxJulianDay"));
        }

        return new BoxJulianDay($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
