<?php declare (strict_types = 1);
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ICAAFile;
use InvalidArgumentException;
use Stringable;

/**
 * BoxFileType class
 *
 * File type. Two-position code. Indicates whether it is a normal shipping file (FL code), or for backorder shipping (AT code)
 * In the event that several overdue files are sent one week, the code will be sequential (01, 02, 03.)
 *
 */
class BoxFileType implements Stringable
{

    const LENGTH = 2;

    /**
     * @var string
     */
    private $value;

    /**
     *  __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length and valid BoxFileType function
     *
     * @param string $value
     * @return BoxFileType
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxFileType
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxFileType"));
        }

        if (!in_array($value, ICAAFile::FILE_TYPE_VALUES)) {
            throw new InvalidArgumentException(sprintf('The value "%s" is not valid in %s', $value, "BoxFileType"));
        }

        return new BoxFileType($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
