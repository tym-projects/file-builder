<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Box;

use InvalidArgumentException;
use Stringable;

/**
 * BoxIncomes Value Object
 *
 * Incomes contained in the file, with 11 digits, filling with leading zeros, if necessary.
 * In 9 position character must be a '.'
 * In 10 and 11 charecters must contain the decimals
 *
 */
class BoxIncomes implements Stringable
{

    const LENGTH = 11;
    const DOT_POSTITION = 9;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length BoxIncomes function
     *
     * @param string $value
     * @return BoxIncomes
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxIncomes
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxIncomes"));
        }

        if (strpos($value, '.') != self::DOT_POSTITION - 1) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong dot position', $value, "BoxIncomes"));
        }

        return new BoxIncomes($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
