<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Box;

use InvalidArgumentException;
use Stringable;

/**
 * BoxSpectators Value Object
 *
 * Total number of viewers included in the file
 * 11 digits, filling with leading zeros, if necessary
 *
 */
class BoxSpectators implements Stringable
{

    const LENGTH = 11;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length BoxSpectators function
     *
     * @param string $value
     * @return BoxSpectators
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxSpectators
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxSpectators"));
        }

        return new BoxSpectators($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
