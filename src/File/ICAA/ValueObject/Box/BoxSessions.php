<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Box;

use InvalidArgumentException;
use Stringable;

/**
 * BoxSessions Value Object
 *
 * Total number of sessions for which information is sent
 * 11 digits filling with leading zeros if necessary
 *
 */
class BoxSessions implements Stringable
{

    const LENGTH = 11;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length BoxSessions function
     *
     * @param string $value
     * @return BoxSessions
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxSessions
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxSessions"));
        }

        return new BoxSessions($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
