<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Box;

use InvalidArgumentException;
use Stringable;

/**
 * BoxFileLines class
 *
 * Total number of lines in the file, with 11 digits, filling with leading zeros, if necessary.
 *
 */
class BoxFileLines implements Stringable
{

    const LENGTH = 11;

    /**
     * @var string
     */
    private $value;

    /**
     * _construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length BoxFileLines function
     *
     * @param string $value
     * @return BoxFileLines
     * @throws InvalidArgumentException
     */
    public static function create(string $value): BoxFileLines
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "BoxFileLines"));
        }

        return new BoxFileLines($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
