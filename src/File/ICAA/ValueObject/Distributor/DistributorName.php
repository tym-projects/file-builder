<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Distributor;

use InvalidArgumentException;
use Stringable;

/**
 * DistributorName class
 *
 * @testFunction testDistributorNameCreate
 */
class DistributorName implements Stringable
{

    const LENGTH = 50;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length DistributorName function
     *
     * @param string $value
     * @return DistributorName
     * @throws InvalidArgumentException
     */
    public static function create(string $value): DistributorName
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "DistributorName"));
        }

        return new DistributorName($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
