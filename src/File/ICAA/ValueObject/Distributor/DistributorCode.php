<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA\ValueObject\Distributor;

use InvalidArgumentException;
use Stringable;

/**
 * DistributorCode class
 *
 * @testFunction testDistributorCode
 */
class DistributorCode implements Stringable
{

    const LENGTH = 12;

    /**
     * @var string
     */
    private $value;

    /**
     * __construct function
     *
     * @param string $value
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Create and test length DistributorCode function
     *
     * @param string $value
     * @return DistributorCode
     * @throws InvalidArgumentException
     */
    public static function create(string $value): DistributorCode
    {
        if (mb_strlen($value) != self::LENGTH) {
            throw new InvalidArgumentException(sprintf('The value "%s" in %s has the wrong length', $value, "DistributorCode"));
        }

        return new DistributorCode($value);
    }

    /**
     * Get the value of value
     *
     * @return  string
     */
    public function __toString(): string
    {
        return $this->value;
    }

}
