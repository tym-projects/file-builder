<?php declare (strict_types = 1);

namespace FileBuilder\File\ICAA;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\FileType;
use FileBuilder\File\ICAA\Entities\HeaderIncident;
use FileBuilder\File\ICAA\Entities\RoomIncident;
use FileBuilder\File\RegisterCollection;

class ICAAIncidentFile implements FileType
{

    /**
     * End Of Line Characters
     */
    const EOL = PHP_EOL;

    /**
     * @var null|HeaderIncident
     */
    private $header;

    /**
     * @var RegisterCollection
     */
    private $roomCollection;

    /**
     * @var string
     */
    private $content;

    public function __construct()
    {

        $this->header = null;
        $this->roomCollection = new RegisterCollection();
        $this->content = "";
    }

    /**
     * @param HeaderIncident $header
     * @return void
     */
    public function addHeader(HeaderIncident $header)
    {
        $this->header = $header;
    }

    /**
     * @param RoomIncident $room
     * @throws CollectionException
     */
    public function addRoom(RoomIncident $room)
    {
        $this->roomCollection->add($room);
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->header->getBoxCode()->__toString() .
        HeaderIncident::FILE_TYPE .
        $this->header->getInitialIncidentDate()->__toString();
    }

    /**
     * @param RegisterCollection $collection
     * @return string
     */
    private function getRegisterCollectionLines(RegisterCollection $collection): string
    {
        $str = "";
        $it = $collection->getIterator();
        for ($it = $collection->getIterator(); $it->valid(); $it->next()) {
            $str .= $it->current()->inLine() . self::EOL;
        }
        return $str;
    }

    /**
     * @return ICAAIncidentFile
     * @throws ValueException
     */
    public function generateContent(): string
    {
        if (is_null($this->header)) {
            throw new ValueException("The Header is null, you must before parse data into the header line");
        }

        $this->content = $this->header->inLine() . self::EOL;
        $this->content .= $this->getRegisterCollectionLines($this->roomCollection);

        return $this->content;

    }

}
