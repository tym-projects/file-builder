<?php declare (strict_types = 1);

namespace FileBuilder\Exception;

use Exception;

final class ValueException extends Exception
{
}
