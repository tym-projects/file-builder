<?php declare (strict_types = 1);

namespace FileBuilder\Exception;

use Exception;

class EncryptFileException extends Exception
{
}
