<?php declare (strict_types = 1);

namespace FileBuilder;

use FileBuilder\Exception\EncryptFileException;
use gnupg;

class GnuPGEncrypter
{

    /**
     * @var gnupg
     */
    private $gnupg;

    /**
     * @param string|null $keyFilePath
     * @return void
     * @throws EncryptFileException
     */
    public function __construct(string $keyFilePath = null)
    {
        if (!is_file((string) $keyFilePath) && !is_null($keyFilePath)) {
            throw new EncryptFileException(sprintf('File %s not found in %s', $keyFilePath, 'GnuPGEncrypter'));
        }

        $this->gnupg = new gnupg();
        $this->gnupg->seterrormode(gnupg::ERROR_EXCEPTION);

        if (!is_null($keyFilePath)) {
            $this->gnupg->import(file_get_contents($keyFilePath));
        }
    }

    /**
     * @param string $data
     * @param string $fingerprintKey
     * @return string
     */
    public function encrypt(string $data, string $fingerprintKey): string
    {
        $this->gnupg->addencryptkey($fingerprintKey);
        return $this->gnupg->encrypt($data);
    }

    /**
     * @param string $data
     * @param string $fingerprintKey
     * @return string
     */
    public function decrypt(string $data, string $fingerprintKey): string
    {
        $this->gnupg->adddecryptkey($fingerprintKey);
        return $this->gnupg->decrypt($data);
    }

    /**
     * @param string $pattern
     * @return array
     */
    public function getKeyInfo(string $pattern): array
    {
        return $this->gnupg->keyinfo($pattern);
    }

}
