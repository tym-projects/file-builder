<?php
namespace FileBuilder;

use FileBuilder\FileBuilder;
use FileBuilder\File\FileType;
use FileBuilder\Storage\StorageSystem;
use PHPUnit\Framework\TestCase;

class FileBuilderTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\FileBuilder
     **/
    public function testFileBuilder()
    {
        $builder = new FileBuilder(
            new Storage(),
            new File()
        );

        $this->assertEquals(FileBuilder::class, get_class($builder));
        $this->assertEquals("This is the content", $builder->saveFile());
    }
}

class Storage implements StorageSystem
{

    public function save(string $fileName, string $fileContent): string
    {
        return $fileContent;
    }
}

class File implements FileType
{

    public function __construct()
    {}

    public function generateContent()
    {
        return "This is the content";
    }

}
