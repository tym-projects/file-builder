<?php
namespace FileBuilder\File\ICAA;

use FileBuilder\Exception\CollectionException;
use FileBuilder\File\ICAA\Entities\EntitieInterface;
use FileBuilder\File\RegisterCollection;
use PHPUnit\Framework\TestCase;

class RegisterCollectionTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\RegisterCollection
     */
    public function testRegisterCollection__construct()
    {
        $registerClass = new RegisterCollection([
            1 => new Fake("This"),
            2 => new Fake("Is"),
            3 => new Fake("Sparta"),
        ]);
        $this->assertEquals(RegisterCollection::class, get_class($registerClass));
        $registerClass->add(new Fake("hello"), 5);
        $registerClass->add(new Fake("bye"), null);
        $this->assertEquals("hello", $registerClass->get(5)->inLine());

    }

    /**
     * @covers FileBuilder\File\RegisterCollection
     **/
    public function testRegisterCollectionCollectionExceptionInConstruct()
    {
        $this->expectException(CollectionException::class);
        new RegisterCollection([1 => "FAKE"]);
    }

    /**
     * @covers FileBuilder\File\RegisterCollection
     **/
    public function testRegisterCollectionCollectionExceptionInAddFunction()
    {
        $this->expectException(CollectionException::class);
        $registerClass = new RegisterCollection([]);
        $registerClass->add(new RegisterCollection([]), null);
    }
}

/**
 * Fake class for test RegiterCollection class
 */
class Fake implements EntitieInterface
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function inLine(): string
    {
        return $this->value;
    }

}
