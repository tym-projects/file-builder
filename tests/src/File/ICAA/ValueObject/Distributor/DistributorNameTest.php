<?php
namespace FileBuilder\File\ICAA\ValueObject\Distributor;

use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorName;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DistributorNameTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Distributor\DistributorName
     **/
    public function testDistributorNameInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        DistributorName::create("654000000  ");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Distributor\DistributorName
     **/
    public function testDistributorNameCreate()
    {
        $value = DistributorName::create("This distributor not exist, you know??            ");

        $this->assertIsString($value->__toString());
        $this->assertEquals("This distributor not exist, you know??            ", $value->__toString());
        $this->assertEquals(get_class($value), DistributorName::class);
    }
}
