<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorCode;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DistributorCodeTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Distributor\DistributorCode
     **/
    public function testDistributorCodeInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        DistributorCode::create("654000000  ");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Distributor\DistributorCode
     **/
    public function testDistributorCode()
    {
        $value = DistributorCode::create("123456789   ");

        $this->assertIsString($value->__toString());
        $this->assertEquals("123456789   ", $value->__toString());
        $this->assertEquals(get_class($value), DistributorCode::class);
    }
}
