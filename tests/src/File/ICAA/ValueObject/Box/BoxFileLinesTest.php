<?php
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ValueObject\Box\BoxFileLines;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxFileLinesTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxFileLines
     **/
    public function testBoxFileLinesCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxFileLines::create(1);
        $value = BoxFileLines::create("897");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxFileLines
     **/
    public function testCorrectBoxFileLinesCreate()
    {
        $value = BoxFileLines::create("00025479631");

        $this->assertIsString($value->__toString());
        $this->assertEquals("00025479631", $value->__toString());
        $this->assertEquals(get_class($value), BoxFileLines::class);
    }
}
