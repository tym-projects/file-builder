<?php
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ValueObject\Box\BoxSessions;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxSessionsTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxSessions
     **/
    public function testBoxSessionsCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxSessions::create(1);
        $value = BoxSessions::create("897");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxSessions
     **/
    public function testCorrectBoxSessionsCreate()
    {
        $value = BoxSessions::create("00958479631");

        $this->assertIsString($value->__toString());
        $this->assertEquals("00958479631", $value->__toString());
        $this->assertEquals(get_class($value), BoxSessions::class);
    }
}
