<?php
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ValueObject\Box\BoxSpectators;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxSpectatorsTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxSpectators
     **/
    public function testBoxSpectatorsCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxSpectators::create(1);
        $value = BoxSpectators::create("897");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxSpectators
     **/
    public function testCorrectBoxSpectatorsCreate()
    {
        $value = BoxSpectators::create("00025479631");

        $this->assertIsString($value->__toString());
        $this->assertEquals("00025479631", $value->__toString());
        $this->assertEquals(get_class($value), BoxSpectators::class);
    }
}
