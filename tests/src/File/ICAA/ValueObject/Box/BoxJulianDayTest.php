<?php
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ValueObject\Box\BoxJulianDay;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxJulianDayTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxJulianDay
     **/
    public function testBoxJulianDayCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxJulianDay::create(1);
        $value = BoxJulianDay::create(98794);
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxJulianDay
     **/
    public function testCorrectBoxJulianDayCreate()
    {
        $value = BoxJulianDay::create("25");

        $this->assertIsString($value->__toString());
        $this->assertEquals("025", $value->__toString());
        $this->assertEquals(get_class($value), BoxJulianDay::class);
    }
}
