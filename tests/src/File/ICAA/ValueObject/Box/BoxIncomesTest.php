<?php
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxIncomesTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes
     **/
    public function testBoxIncomesCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxIncomes::create(1);
        $value = BoxIncomes::create("897");
        $value = BoxIncomes::create("123456987.1");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes
     **/
    public function testBoxIncomesCreateExceptionDotInEspecificPosition()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxIncomes::create("123456987.1");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes
     **/
    public function testCorrectBoxIncomesCreate()
    {
        $value = BoxIncomes::create("00025479.31");

        $this->assertIsString($value->__toString());
        $this->assertEquals("00025479.31", $value->__toString());
        $this->assertEquals(get_class($value), BoxIncomes::class);
    }
}
