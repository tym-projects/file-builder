<?php
namespace FileBuilder\File\ICAA\ValueObject\Box;

use FileBuilder\File\ICAA\ValueObject\Box\BoxFileType;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxFileTypeTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxFileType
     **/
    public function testBoxFileTypeCreateExceptionFromLength()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxFileType::create(1);
        $value = BoxFileType::create("897");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxFileType
     **/
    public function testBoxFileTypeCreateExceptionValidArgumentList()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxFileType::create("07");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Box\BoxFileType
     **/
    public function testCorrectBoxFileTypeCreate()
    {
        $value = BoxFileType::create("FL");

        $this->assertIsString($value->__toString());
        $this->assertEquals("FL", $value->__toString());
        $this->assertEquals(get_class($value), BoxFileType::class);
    }
}
