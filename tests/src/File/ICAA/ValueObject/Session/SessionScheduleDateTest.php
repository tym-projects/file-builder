<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\File\ICAA\ValueObject\Session\SessionScheduleDate;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionScheduleDateTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionScheduleDate
     **/
    public function testSessionScheduleDateInvalidDateException()
    {
        $this->expectException(InvalidArgumentException::class);
        SessionScheduleDate::create("1254789");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionScheduleDate
     **/
    public function testSessionScheduleDate()
    {
        $date = SessionScheduleDate::create("2021-01-12");

        $this->assertIsString($date->__toString());
        $this->assertEquals("120121", $date->__toString());
        $this->assertEquals(SessionScheduleDate::class, get_class($date));
    }
}
