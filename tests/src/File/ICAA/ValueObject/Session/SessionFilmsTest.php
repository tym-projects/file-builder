<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\File\ICAA\ValueObject\Session\SessionFilms;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionFilmsTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionFilms
     **/
    public function testSessionFilmsInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);

        SessionFilms::create(1);
        SessionFilms::create("111");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionFilms
     **/
    public function testSessionFilms()
    {
        $value = SessionFilms::create(12);

        $this->assertIsString($value->__toString());
        $this->assertEquals("12", $value->__toString());
        $this->assertEquals(SessionFilms::class, get_class($value));
    }
}
