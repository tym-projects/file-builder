<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\File\ICAA\ValueObject\Session\SessionSpectators;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionSpectatorsTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionSpectators
     **/
    public function testSessionSpectatorsInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);

        SessionSpectators::create("2544");
        SessionSpectators::create("2544654");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionSpectators
     **/
    public function testSessionSpectators()
    {
        $value = SessionSpectators::create("00451");

        $this->assertIsString($value->__toString());
        $this->assertEquals("00451", $value->__toString());
        $this->assertEquals(SessionSpectators::class, get_class($value));
    }
}
