<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\File\ICAA\ValueObject\Session\SessionDate;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionDateTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionDate::create
     **/
    public function testSessionDateInvalidDateException()
    {
        $this->expectException(InvalidArgumentException::class);
        SessionDate::create(1254789);
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionDate
     **/
    public function testSessionDate()
    {
        $date = SessionDate::create("2021-01-12 12:15");

        $this->assertIsString($date->__toString());
        $this->assertEquals("1201211215", $date->__toString());
        $this->assertEquals(SessionDate::class, get_class($date));
    }
}
