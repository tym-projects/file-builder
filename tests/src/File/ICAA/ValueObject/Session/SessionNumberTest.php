<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\File\ICAA\ValueObject\Session\SessionNumber;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionNumberTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionNumber
     **/
    public function testSessionNumberInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        SessionNumber::create("654654");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionNumber
     **/
    public function testSessionNumberCreate()
    {
        $value = SessionNumber::create("08");

        $this->assertIsString($value->__toString());
        $this->assertEquals("08", $value->__toString());
        $this->assertEquals(get_class($value), SessionNumber::class);
    }
}
