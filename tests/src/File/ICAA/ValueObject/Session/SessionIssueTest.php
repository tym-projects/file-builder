<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIssue;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionIssueTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionIssue
     **/
    public function testSessionIssueInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        SessionIssue::create("654654");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionIssue
     **/
    public function testSessionIssueValueException()
    {
        $this->expectException(ValueException::class);
        SessionIssue::create("321");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionIssue
     **/
    public function testSessionIssue()
    {
        $value = SessionIssue::create(SessionIssue::MS_CANCEL);

        $this->assertIsString($value->__toString());
        $this->assertEquals(SessionIssue::MS_CANCEL, $value->__toString());
        $this->assertEquals(get_class($value), SessionIssue::class);
    }
}
