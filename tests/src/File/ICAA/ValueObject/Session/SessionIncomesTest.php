<?php
namespace FileBuilder\File\ICAA\ValueObject\Session;

use FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class SessionIncomesTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes
     **/
    public function testSessionIncomesInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        SessionIncomes::create("124124029.31");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes
     **/
    public function testSessionIncomesDotInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        SessionIncomes::create("029000.1");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes
     **/
    public function testSessionIncomes()
    {
        $value = SessionIncomes::create("02479.31");

        $this->assertIsString($value->__toString());
        $this->assertEquals("02479.31", $value->__toString());
        $this->assertEquals(get_class($value), SessionIncomes::class);
    }
}
