<?php
namespace PHPTDD\src\File\ICAA\ValueObject\Incident;

use FileBuilder\File\ICAA\ValueObject\Incident\IncidentComment;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class IncidentCommentTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\IncidentComment
     **/
    public function testBoxCodeCreateExceptionMaxValueLenght()
    {
        $this->expectException(InvalidArgumentException::class);
        IncidentComment::create("this string is too long because i need to throw a InvalidArgumentException, You know ?");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\IncidentComment
     **/
    public function testIncidentComment()
    {
        $value = IncidentComment::create("Hello my Friend");

        $this->assertIsString($value->__toString());
        $this->assertEquals("Hello my Friend", $value->__toString());
        $this->assertEquals(get_class($value), IncidentComment::class);
    }
}
