<?php
namespace PHPTDD\src\File\ICAA\ValueObject\Incident;

use FileBuilder\File\ICAA\ValueObject\Incident\HasIssue;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class HasIssueTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\HasIssue
     **/
    public function testHasIssueCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = HasIssue::create(25);
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\HasIssue
     **/
    public function testHasIssue()
    {
        $value = HasIssue::create("1");

        $this->assertIsString($value->__toString());
        $this->assertEquals("1", $value->__toString());
        $this->assertEquals(get_class($value), HasIssue::class);
    }
}
