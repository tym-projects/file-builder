<?php
namespace PHPTDD\src\File\ICAA\ValueObject\Incident;

use FileBuilder\File\ICAA\ValueObject\Incident\IncidentDate;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class IncidentDateTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\IncidentDate
     **/
    public function testBoxCodeCreateExceptionMaxValueLenght()
    {
        $this->expectException(InvalidArgumentException::class);
        IncidentDate::create("this is an not valid input");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\IncidentDate
     **/
    public function testIncidentDate()
    {
        $value = IncidentDate::create("2021-12-12");

        $this->assertIsString($value->__toString());
        $this->assertEquals("121221", $value->__toString());
        $this->assertEquals(get_class($value), IncidentDate::class);
    }
}
