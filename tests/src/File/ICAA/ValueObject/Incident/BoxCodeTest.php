<?php
namespace PHPTDD\src\File\ICAA\ValueObject\Incident;

use FileBuilder\File\ICAA\ValueObject\Incident\BoxCode;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BoxCodeTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\BoxCode
     **/
    public function testBoxCodeCreateException()
    {
        $this->expectException(InvalidArgumentException::class);
        $value = BoxCode::create(25);
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Incident\BoxCode
     **/
    public function testBoxCode()
    {
        $value = BoxCode::create(278);

        $this->assertIsString($value->__toString());
        $this->assertEquals("278", $value->__toString());
        $this->assertEquals(get_class($value), BoxCode::class);
    }
}
