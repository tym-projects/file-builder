<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\File\ICAA\ValueObject\Film\FilmTitle;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmTitleTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmTitle
     **/
    public function testFilmTitleInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmTitle::create("The movie that I like when I go to sleep 1");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmTitle
     **/
    public function testFilmTitle()
    {
        $value = FilmTitle::create("The movie that I like when I go to sleep 2        ");

        $this->assertIsString($value->__toString());
        $this->assertEquals("The movie that I like when I go to sleep 2        ", $value->__toString());
        $this->assertEquals(get_class($value), FilmTitle::class);
    }
}
