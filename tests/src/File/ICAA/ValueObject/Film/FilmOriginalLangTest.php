<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmOriginalLangTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang
     **/
    public function testFilmOriginalLangInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmOriginalLang::create("12");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang
     **/
    public function testFilmOriginalLangValueException()
    {
        $this->expectException(ValueException::class);
        FilmOriginalLang::create("F");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang
     **/
    public function testFilmOriginalLang()
    {
        $value = FilmOriginalLang::create(FilmOriginalLang::GALLEGO);

        $this->assertIsString($value->__toString());
        $this->assertEquals(FilmOriginalLang::GALLEGO, $value->__toString());
        $this->assertEquals(get_class($value), FilmOriginalLang::class);
    }
}
