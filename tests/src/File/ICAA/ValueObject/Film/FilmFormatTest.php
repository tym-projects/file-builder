<?php
namespace PHPTDD\src\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\ICAA\ValueObject\Film\FilmFormat;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmFormatTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmFormat
     **/
    public function testFilmOriginalLangInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmFormat::create("12");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmFormat
     **/
    public function testFilmOriginalLangValueException()
    {
        $this->expectException(ValueException::class);
        FilmFormat::create("F");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmFormat
     **/
    public function testFilmFormat()
    {
        $value = FilmFormat::create(FilmFormat::DVD);

        $this->assertIsString($value->__toString());
        $this->assertEquals(FilmFormat::DVD, $value->__toString());
        $this->assertEquals(get_class($value), FilmFormat::class);
    }
}
