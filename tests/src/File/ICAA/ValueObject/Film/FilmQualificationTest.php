<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\File\ICAA\ValueObject\Film\FilmQualification;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmQualificationTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmQualification
     **/
    public function testBoxCodeCreateInvalidValueException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmQualification::create("kajsfaf");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmQualification
     **/
    public function testFilmQualification()
    {
        $value = FilmQualification::create("654654000000");

        $this->assertIsString($value->__toString());
        $this->assertEquals("654654000000", $value->__toString());
        $this->assertEquals(get_class($value), FilmQualification::class);
    }
}
