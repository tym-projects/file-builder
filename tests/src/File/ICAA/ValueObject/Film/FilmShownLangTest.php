<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmShownLangTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang
     **/
    public function testFilmShownLangInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmShownLang::create("12");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang
     **/
    public function testFilmShownLangValueException()
    {
        $this->expectException(ValueException::class);
        FilmShownLang::create("Ñ");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang
     **/
    public function testFilmShownLang()
    {
        $value = FilmShownLang::create(FilmShownLang::DUBBING_CATALAN);

        $this->assertIsString($value->__toString());
        $this->assertEquals(FilmShownLang::DUBBING_CATALAN, $value->__toString());
        $this->assertEquals(get_class($value), FilmShownLang::class);
    }
}
