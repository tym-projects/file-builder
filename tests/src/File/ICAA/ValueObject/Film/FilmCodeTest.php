<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\File\ICAA\ValueObject\Film\FilmCode;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmCodeTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmCode
     **/
    public function testBoxCodeCreateInvalidValueException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmCode::create("kajsfaf");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmCode
     **/
    public function testFilmCode()
    {
        $value = FilmCode::create("00025");

        $this->assertIsString($value->__toString());
        $this->assertEquals("00025", $value->__toString());
        $this->assertEquals(get_class($value), FilmCode::class);
    }
}
