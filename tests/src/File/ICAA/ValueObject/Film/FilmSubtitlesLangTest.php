<?php
namespace FileBuilder\File\ICAA\ValueObject\Film;

use FileBuilder\Exception\ValueException;
use FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FilmSubtitlesLangTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang
     **/
    public function testFilmShownLangInvalidArgumentException()
    {
        $this->expectException(InvalidArgumentException::class);
        FilmSubtitlesLang::create("A1");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang
     **/
    public function testFilmShownLangValueException()
    {
        $this->expectException(ValueException::class);
        FilmSubtitlesLang::create("Ñ");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang
     **/
    public function testFilmSubtitlesLang()
    {
        $value = FilmSubtitlesLang::create(FilmSubtitlesLang::NO_SUBTITLES);

        $this->assertIsString($value->__toString());
        $this->assertEquals(FilmSubtitlesLang::NO_SUBTITLES, $value->__toString());
        $this->assertEquals(get_class($value), FilmSubtitlesLang::class);
    }
}
