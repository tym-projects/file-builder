<?php
namespace FileBuilder\File\ICAA\ValueObject\Room;

use FileBuilder\File\ICAA\ValueObject\Room\RoomName;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class RoomNameTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Room\RoomName
     **/
    public function testRoomNameCreateExceptionValidArgumentList()
    {
        $this->expectException(InvalidArgumentException::class);
        RoomName::create("Sala 1");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Room\RoomName
     **/
    public function testRoomName()
    {
        $str = "Sala1 del Cine de Alcañiz     ";

        $value = RoomName::create($str);

        $this->assertIsString($value->__toString());
        $this->assertEquals($str, $value->__toString());
        $this->assertEquals(RoomName::class, get_class($value));
    }
}
