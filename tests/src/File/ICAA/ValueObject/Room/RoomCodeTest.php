<?php
namespace FileBuilder\File\ICAA\ValueObject\Room;

use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class RoomCodeTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Room\RoomCode
     **/
    public function testRoomCodeCreateExceptionValidArgumentList()
    {
        $this->expectException(InvalidArgumentException::class);
        RoomCode::create("07  11 ");
    }

    /**
     * @covers FileBuilder\File\ICAA\ValueObject\Room\RoomCode
     **/
    public function testRoomCode()
    {
        $value = RoomCode::create('6847D4      ');

        $this->assertIsString($value->__toString());
        $this->assertEquals('6847D4      ', $value->__toString());
        $this->assertEquals(RoomCode::class, get_class($value));
    }
}
