<?php
namespace PHPTDD\src\File\ICAA;

use FileBuilder\File\ICAA\ICAADataProvider;
use PHPUnit\Framework\TestCase;

class ICAADataProviderTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ICAADataProvider
     **/
    public function testICAADataProvider()
    {
        $provider = new ICAADataProvider();

        $this->assertIsArray($provider->getFilmsData());
        $this->assertIsArray($provider->getRoomIncidents("2021-01-01", "2021-01-07"));
        $this->assertIsArray($provider->getRoomsData());
        $this->assertIsArray($provider->getSessionFilmsData());
        $this->assertIsArray($provider->getSessionsData());
        $this->assertIsArray($provider->getSessionsSchedules());
    }
}
