<?php
namespace FileBuilder\File\ICAA;

use FileBuilder\File\ICAA\Entities\Box;
use FileBuilder\File\ICAA\Entities\Film;
use FileBuilder\File\ICAA\Entities\Room;
use FileBuilder\File\ICAA\Entities\Session;
use FileBuilder\File\ICAA\Entities\SessionFilm;
use FileBuilder\File\ICAA\Entities\SessionSchedule;
use FileBuilder\File\ICAA\ICAAFile;
use FileBuilder\File\RegisterCollection;
use PHPUnit\Framework\TestCase;

class ICAAFileTest extends TestCase
{

    private $c;
    private $fake;
    private $box;
    private $room;
    private $session;
    private $film;
    private $sessionfilm;
    private $sessionschedule;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->c = $this->getMockBuilder(RegisterCollection::class)->disableOriginalConstructor()->getMock();
        $this->c->method('count')->willReturn(0);

        $this->fake = $this->getMockBuilder(RegisterCollection::class)->disableOriginalConstructor()->getMock();
        $this->fake->method('count')->willReturn(1);

        $this->box = $this->getMockBuilder(Box::class)->disableOriginalConstructor()->getMock();
        $this->box->method('inLine')->willReturn('box');

        $this->room = $this->getMockBuilder(Room::class)->disableOriginalConstructor()->getMock();
        $this->room->method('inLine')->willReturn('room');

        $this->film = $this->getMockBuilder(Film::class)->disableOriginalConstructor()->getMock();
        $this->film->method('inLine')->willReturn('film');

        $this->session = $this->getMockBuilder(Session::class)->disableOriginalConstructor()->getMock();
        $this->session->method('inLine')->willReturn('session');

        $this->sessionfilm = $this->getMockBuilder(SessionFilm::class)->disableOriginalConstructor()->getMock();
        $this->sessionfilm->method('inLine')->willReturn('sessionfilm');

        $this->sessionschedule = $this->getMockBuilder(SessionSchedule::class)->disableOriginalConstructor()->getMock();
        $this->sessionschedule->method('inLine')->willReturn('sessionschedule');
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\ICAAFile
     * @covers FileBuilder\File\Collection
     * @covers FileBuilder\File\RegisterCollection
     **/
    public function testICAAFile()
    {
        $file = new ICAAFile();
        $file->addBox($this->box);
        $file->addRoom($this->room);
        $file->addFilm($this->film);
        $file->addSession($this->session);
        $file->addSessionFilm($this->sessionfilm);
        $file->addSessionSchedule($this->sessionschedule);

        $this->assertEquals(ICAAFile::class, get_class($file));
        $this->assertEquals(5, $file->countLines());
    }
}
