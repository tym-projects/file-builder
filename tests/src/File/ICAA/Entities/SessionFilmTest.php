<?php
namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\SessionFilm;
use FileBuilder\File\ICAA\ValueObject\Film\FilmCode;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Session\SessionDate;
use PHPUnit\Framework\TestCase;

class SessionFilmTest extends TestCase
{

    private $RoomCode;
    private $SessionDate;
    private $FilmCode;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->RoomCode = $this->getMockBuilder(RoomCode::class)->disableOriginalConstructor()->getMock();
        $this->RoomCode->method("__toString")->willReturn("123456      ");

        $this->SessionDate = $this->getMockBuilder(SessionDate::class)->disableOriginalConstructor()->getMock();
        $this->SessionDate->method("__toString")->willReturn(date(SessionDate::DATE_FORMAT, strtotime("2021-01-12 20:30")));

        $this->FilmCode = $this->getMockBuilder(FilmCode::class)->disableOriginalConstructor()->getMock();
        $this->FilmCode->method("__toString")->willReturn("00025");
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\SessionFilm
     **/
    public function testSessionFilm()
    {
        $sessionFilm = new SessionFilm(
            $this->RoomCode,
            $this->SessionDate,
            $this->FilmCode
        );

        $this->assertEquals(
            "3123456      120121203000025",
            $sessionFilm->inLine()
        );
        $this->assertEquals(SessionFilm::class, get_class($sessionFilm));
        $this->assertEquals(SessionFilm::LINE_LENGHT, mb_strlen($sessionFilm->inLine()));
    }
}
