<?php
namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\Session;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Session\SessionDate;
use FileBuilder\File\ICAA\ValueObject\Session\SessionFilms;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIncomes;
use FileBuilder\File\ICAA\ValueObject\Session\SessionIssue;
use FileBuilder\File\ICAA\ValueObject\Session\SessionSpectators;
use PHPUnit\Framework\TestCase;

class SessionTest extends TestCase
{

    private $RoomCode;
    private $SessionDate;
    private $SessionFilms;
    private $SessionSpectators;
    private $SessionIncomes;
    private $SessionIssue;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->RoomCode = $this->getMockBuilder(RoomCode::class)->disableOriginalConstructor()->getMock();
        $this->RoomCode->method("__toString")->willReturn("123456      ");

        $this->SessionDate = $this->getMockBuilder(SessionDate::class)->disableOriginalConstructor()->getMock();
        $this->SessionDate->method("__toString")->willReturn(date(SessionDate::DATE_FORMAT, strtotime("2021-01-12 20:30")));

        $this->SessionFilms = $this->getMockBuilder(SessionFilms::class)->disableOriginalConstructor()->getMock();
        $this->SessionFilms->method("__toString")->willReturn("02");

        $this->SessionSpectators = $this->getMockBuilder(SessionSpectators::class)->disableOriginalConstructor()->getMock();
        $this->SessionSpectators->method("__toString")->willReturn("00112");

        $this->SessionIncomes = $this->getMockBuilder(SessionIncomes::class)->disableOriginalConstructor()->getMock();
        $this->SessionIncomes->method("__toString")->willReturn("00125.50");

        $this->SessionIssue = $this->getMockBuilder(SessionIssue::class)->disableOriginalConstructor()->getMock();
        $this->SessionIssue->method("__toString")->willReturn(SessionIssue::NO_ISSUE);

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\Session
     **/
    public function testSession()
    {
        $session = new Session(
            $this->RoomCode,
            $this->SessionDate,
            $this->SessionFilms,
            $this->SessionSpectators,
            $this->SessionIncomes,
            $this->SessionIssue
        );

        $this->assertEquals(
            "2123456      1201212030020011200125.50000",
            $session->inLine()
        );
        $this->assertEquals(Session::class, get_class($session));
        $this->assertEquals(Session::LINE_LENGHT, mb_strlen($session->inLine()));
        $this->assertEquals("00112", $session->getSpectators());
        $this->assertEquals("00125.50", $session->getIncomes());
    }
}
