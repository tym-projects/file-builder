<?php
namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\Room;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Room\RoomName;
use PHPUnit\Framework\TestCase;

class RoomTest extends TestCase
{

    private $RoomCode;
    private $RoomName;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->RoomCode = $this->getMockBuilder(RoomCode::class)->disableOriginalConstructor()->getMock();
        $this->RoomCode->method("__toString")->willReturn("123456      ");
        $this->RoomName = $this->getMockBuilder(RoomName::class)->disableOriginalConstructor()->getMock();
        $this->RoomName->method("__toString")->willReturn("Sala Primera alcañiz          ");
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\Room
     **/
    public function testRoomInLine()
    {
        $room = new Room(
            $this->RoomCode,
            $this->RoomName
        );

        $this->assertEquals(
            "1123456      Sala Primera alcañiz          ",
            $room->inLine()
        );
        $this->assertEquals(Room::class, get_class($room));
        $this->assertEquals(Room::LINE_LENGHT, mb_strlen($room->inLine()));

    }
}
