<?php
namespace PHPTDD\src\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\HeaderIncident;
use FileBuilder\File\ICAA\ValueObject\Incident\BoxCode;
use FileBuilder\File\ICAA\ValueObject\Incident\IncidentDate;
use PHPUnit\Framework\TestCase;

class HeaderIncidentTest extends TestCase
{

    private $BoxCode;
    private $initialIncidentDate;
    private $finalIncidentDate;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->BoxCode = $this->getMockBuilder(BoxCode::class)->disableOriginalConstructor()->getMock();
        $this->BoxCode->method("__toString")->willReturn("987");
        $this->initialIncidentDate = $this->getMockBuilder(IncidentDate::class)->disableOriginalConstructor()->getMock();
        $this->initialIncidentDate->method("__toString")->willReturn("01012021");
        $this->finalIncidentDate = $this->getMockBuilder(IncidentDate::class)->disableOriginalConstructor()->getMock();
        $this->finalIncidentDate->method("__toString")->willReturn("01012021");

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\HeaderIncident
     **/
    public function testHeaderIncident()
    {
        $header = new HeaderIncident(
            $this->BoxCode,
            $this->initialIncidentDate,
            $this->finalIncidentDate
        );

        $this->assertEquals("09870101202101012021", $header->inLine());
        $this->assertEquals("987", $header->getBoxCode());
        $this->assertEquals("01012021", $header->getInitialIncidentDate());

    }
}
