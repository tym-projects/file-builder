<?php
namespace PHPTDD\src\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\RoomIncident;
use FileBuilder\File\ICAA\ValueObject\Incident\HasIssue;
use FileBuilder\File\ICAA\ValueObject\Incident\IncidentComment;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use PHPUnit\Framework\TestCase;

class RoomIncidentTest extends TestCase
{

    private $RoomCode;
    private $HasIssue;
    private $IncidentComment;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->RoomCode = $this->getMockBuilder(RoomCode::class)->disableOriginalConstructor()->getMock();
        $this->RoomCode->method("__toString")->willReturn("123456      ");
        $this->HasIssue = $this->getMockBuilder(HasIssue::class)->disableOriginalConstructor()->getMock();
        $this->HasIssue->method("__toString")->willReturn("1");
        $this->IncidentComment = $this->getMockBuilder(IncidentComment::class)->disableOriginalConstructor()->getMock();
        $this->IncidentComment->method("__toString")->willReturn("Comment String");

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\RoomIncident
     **/
    public function testRoomIncident()
    {
        $incident = new RoomIncident(
            $this->RoomCode,
            $this->HasIssue,
            $this->HasIssue,
            $this->HasIssue,
            $this->HasIssue,
            $this->HasIssue,
            $this->HasIssue,
            $this->HasIssue,
            $this->IncidentComment
        );

        $this->assertEquals(
            RoomIncident::REGISTER_TYPE . "123456      1111111Comment String",
            $incident->inLine()
        );
    }
}
