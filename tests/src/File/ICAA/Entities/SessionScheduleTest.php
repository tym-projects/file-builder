<?php
namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\SessionSchedule;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use FileBuilder\File\ICAA\ValueObject\Session\SessionNumber;
use FileBuilder\File\ICAA\ValueObject\Session\SessionScheduleDate;
use PHPUnit\Framework\TestCase;

class SessionScheduleTest extends TestCase
{

    private $RoomCode;
    private $SessionScheduleDate;
    private $SessionNumber;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->RoomCode = $this->getMockBuilder(RoomCode::class)->disableOriginalConstructor()->getMock();
        $this->RoomCode->method("__toString")->willReturn("123456      ");

        $this->SessionScheduleDate = $this->getMockBuilder(SessionScheduleDate::class)->disableOriginalConstructor()->getMock();
        $this->SessionScheduleDate->method("__toString")->willReturn(date(SessionScheduleDate::DATE_FORMAT, strtotime("2021-01-12")));

        $this->SessionNumber = $this->getMockBuilder(SessionNumber::class)->disableOriginalConstructor()->getMock();
        $this->SessionNumber->method("__toString")->willReturn("08");
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\SessionSchedule
     **/
    public function testSessionScheduleInLine()
    {
        $sessionSchedule = new SessionSchedule(
            $this->RoomCode,
            $this->SessionScheduleDate,
            $this->SessionNumber
        );

        $this->assertEquals(
            "5123456      12012108",
            $sessionSchedule->inLine()
        );
        $this->assertEquals(SessionSchedule::class, get_class($sessionSchedule));
        $this->assertEquals(SessionSchedule::LINE_LENGHT, mb_strlen($sessionSchedule->inLine()));
    }
}
