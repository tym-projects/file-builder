<?php
namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\Box;
use FileBuilder\File\ICAA\ValueObject\Box\BoxCode;
use FileBuilder\File\ICAA\ValueObject\Box\BoxFileLines;
use FileBuilder\File\ICAA\ValueObject\Box\BoxFileType;
use FileBuilder\File\ICAA\ValueObject\Box\BoxIncomes;
use FileBuilder\File\ICAA\ValueObject\Box\BoxJulianDay;
use FileBuilder\File\ICAA\ValueObject\Box\BoxSessions;
use FileBuilder\File\ICAA\ValueObject\Box\BoxSpectators;
use PHPUnit\Framework\TestCase;

class BoxTest extends TestCase
{

    private $BoxCode;
    private $BoxFileLines;
    private $BoxFileType;
    private $BoxIncomes;
    private $BoxJulianDay;
    private $BoxSessions;
    private $BoxSpectators;
    private $BoxIncomesFail;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->BoxCode = $this->getMockBuilder(BoxCode::class)->disableOriginalConstructor()->getMock();
        $this->BoxCode->method("__toString")->willReturn("857");

        $this->BoxFileType = $this->getMockBuilder(BoxFileType::class)->disableOriginalConstructor()->getMock();
        $this->BoxFileType->method("__toString")->willReturn("FL");

        $this->BoxJulianDay = $this->getMockBuilder(BoxJulianDay::class)->disableOriginalConstructor()->getMock();
        $this->BoxJulianDay->method("__toString")->willReturn("112");

        $this->BoxFileLines = $this->getMockBuilder(BoxFileLines::class)->disableOriginalConstructor()->getMock();
        $this->BoxFileLines->method("__toString")->willReturn("00000012587");

        $this->BoxSessions = $this->getMockBuilder(BoxSessions::class)->disableOriginalConstructor()->getMock();
        $this->BoxSessions->method("__toString")->willReturn("00000000258");

        $this->BoxSpectators = $this->getMockBuilder(BoxSpectators::class)->disableOriginalConstructor()->getMock();
        $this->BoxSpectators->method("__toString")->willReturn("00000001122");

        $this->BoxIncomes = $this->getMockBuilder(BoxIncomes::class)->disableOriginalConstructor()->getMock();
        $this->BoxIncomesFail = $this->BoxIncomes;
        $this->BoxIncomes->method("__toString")->willReturn("00002589.50");
        $this->BoxIncomesFail->method("__toString")->willReturn("00589.50");

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\Box::inLine
     * @covers FileBuilder\File\ICAA\Entities\Box::__construct
     **/
    public function testBoxInLine()
    {

        $box = new Box(
            $this->BoxCode,
            $this->BoxFileType,
            $this->BoxJulianDay,
            $this->BoxJulianDay,
            $this->BoxFileLines,
            $this->BoxSessions,
            $this->BoxSpectators,
            $this->BoxIncomes
        );

        $this->assertEquals(get_class($box), Box::class);
        $this->assertEquals(
            "0857FL11211200000012587000000002580000000112200002589.50",
            $box->inLine()
        );
        $this->assertEquals(Box::LINE_LENGHT, mb_strlen($box->inLine()));
    }
}
