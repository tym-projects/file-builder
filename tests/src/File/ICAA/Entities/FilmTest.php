<?php
namespace FileBuilder\File\ICAA\Entities;

use FileBuilder\File\ICAA\Entities\Film;
use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorCode;
use FileBuilder\File\ICAA\ValueObject\Distributor\DistributorName;
use FileBuilder\File\ICAA\ValueObject\Film\FilmCode;
use FileBuilder\File\ICAA\ValueObject\Film\FilmFormat;
use FileBuilder\File\ICAA\ValueObject\Film\FilmOriginalLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmQualification;
use FileBuilder\File\ICAA\ValueObject\Film\FilmShownLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmSubtitlesLang;
use FileBuilder\File\ICAA\ValueObject\Film\FilmTitle;
use FileBuilder\File\ICAA\ValueObject\Room\RoomCode;
use PHPUnit\Framework\TestCase;

class FilmTest extends TestCase
{

    private $RoomCode;
    private $FilmCode;
    private $FilmQualification;
    private $FilmTitle;
    private $DistributorCode;
    private $DistributorName;
    private $FilmOriginalLang;
    private $FilmShownLang;
    private $FilmSubtitlesLang;
    private $FilmFormat;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->RoomCode = $this->getMockBuilder(RoomCode::class)->disableOriginalConstructor()->getMock();
        $this->RoomCode->method("__toString")->willReturn("123456      ");
        $this->FilmCode = $this->getMockBuilder(FilmCode::class)->disableOriginalConstructor()->getMock();
        $this->FilmCode->method("__toString")->willReturn("12345");
        $this->FilmQualification = $this->getMockBuilder(FilmQualification::class)->disableOriginalConstructor()->getMock();
        $this->FilmQualification->method("__toString")->willReturn("22584654    ");
        $this->FilmTitle = $this->getMockBuilder(FilmTitle::class)->disableOriginalConstructor()->getMock();
        $this->FilmTitle->method("__toString")->willReturn("The movie that I like when I go to sleep 2        ");
        $this->DistributorCode = $this->getMockBuilder(DistributorCode::class)->disableOriginalConstructor()->getMock();
        $this->DistributorCode->method("__toString")->willReturn("1234        ");
        $this->DistributorName = $this->getMockBuilder(DistributorName::class)->disableOriginalConstructor()->getMock();
        $this->DistributorName->method("__toString")->willReturn("This distributor not exist, you know??            ");
        $this->FilmOriginalLang = $this->getMockBuilder(FilmOriginalLang::class)->disableOriginalConstructor()->getMock();
        $this->FilmOriginalLang->method("__toString")->willReturn(FilmOriginalLang::SPANISH);
        $this->FilmShownLang = $this->getMockBuilder(FilmShownLang::class)->disableOriginalConstructor()->getMock();
        $this->FilmShownLang->method("__toString")->willReturn(FilmShownLang::DUBBING_SPANISH);
        $this->FilmSubtitlesLang = $this->getMockBuilder(FilmSubtitlesLang::class)->disableOriginalConstructor()->getMock();
        $this->FilmSubtitlesLang->method("__toString")->willReturn(FilmSubtitlesLang::NO_SUBTITLES);
        $this->FilmFormat = $this->getMockBuilder(FilmFormat::class)->disableOriginalConstructor()->getMock();
        $this->FilmFormat->method("__toString")->willReturn(FilmFormat::DIGITAL2D);
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\ICAA\Entities\Film
     **/
    public function testFilm()
    {
        $film = new Film(
            $this->RoomCode,
            $this->FilmCode,
            $this->FilmQualification,
            $this->FilmTitle,
            $this->DistributorCode,
            $this->DistributorName,
            $this->FilmOriginalLang,
            $this->FilmShownLang,
            $this->FilmSubtitlesLang,
            $this->FilmFormat
        );

        $this->assertEquals(get_class($film), Film::class);
        $this->assertEquals(
            "4123456      1234522584654    The movie that I like when I go to sleep 2" .
            "        1234        This distributor not exist, you know??            1W01",
            $film->inLine()
        );
        $this->assertEquals(Film::LINE_LENGHT, mb_strlen($film->inLine()));
    }
}
