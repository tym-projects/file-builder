<?php
namespace FileBuilder\File;

use ArrayIterator;
use FileBuilder\File\Collection;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    private $expected;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->expected = [
            '1' => 'foo',
            '2' => 'bar',
            '3' => 'foobar',
        ];
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\File\Collection
     **/
    public function testCollection()
    {
        $collection = new Fake($this->expected);
        $this->assertEquals($this->expected, $collection->getItems());
        $this->assertEquals(3, $collection->count());

        $collection->remove([1, 2]);
        $this->assertNotEquals($this->expected, $collection->getItems());
        $this->assertEquals(1, $collection->count());
        $this->assertEquals('foobar', $collection->get(3));

        $this->assertEquals(ArrayIterator::class, \get_class($collection->getIterator()));
    }
}

class Fake extends Collection
{

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function add(mixed $item, ?int $key = null)
    {

        if (\is_null($key)) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
    }

    protected function ValidateItem($item)
    {
        return true;
    }

    public function get($key)
    {
        return $this->items[$key];
    }

}
