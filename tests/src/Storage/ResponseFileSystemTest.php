<?php
namespace FileBuilder\Storage;

use FileBuilder\Storage\ResponseFileSystem;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class ResponseFileSystemTest extends TestCase
{

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {

    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\Storage\ResponseFileSystem
     **/
    public function testResponseFileSystem()
    {
        $responseFile = new ResponseFileSystem(
            new FakeResponse()
        );

        $this->assertEquals(ResponseFileSystem::class, get_class($responseFile));
        $this->assertEquals(FakeResponse::class, get_class($responseFile->save("hello", "hi")));
    }
}

class FakeResponse implements ResponseInterface
{
    public function __construct()
    {}

    public function getStatusCode()
    {}

    public function withStatus($code, $reasonPhrase = '')
    {}

    public function getReasonPhrase()
    {}

    public function getProtocolVersion()
    {}

    public function withProtocolVersion($version)
    {}

    public function getHeaders()
    {}

    public function hasHeader($name)
    {}

    public function getHeader($name)
    {}

    public function getHeaderLine($name)
    {}

    public function withHeader($name, $value): ResponseInterface
    {return new FakeResponse();}

    public function withAddedHeader($name, $value)
    {}

    public function withoutHeader($name)
    {}

    public function getBody(): self
    {return $this;}

    public function withBody(StreamInterface $body)
    {}

    public function write(string $file)
    {}

}
