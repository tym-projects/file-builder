<?php
namespace PHPTDD\src\Storage;

use Exception;
use FileBuilder\Storage\LocalFileSystem;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class LocalFileSystemTest extends TestCase
{

    private $path;
    private $path_not_exist;

    /**
     * This code will run before each test executes
     * @return void
     */
    protected function setUp(): void
    {
        $this->path = __DIR__ . "/TestFiles/";
        $this->path_not_exist = "NotFolder";
    }

    /**
     * This code will run after each test executes
     * @return void
     */
    protected function tearDown(): void
    {

    }

    /**
     * @covers FileBuilder\Storage\LocalFileSystem
     **/
    public function testLocalFileSystemInvalidFolderException()
    {
        $this->expectException(InvalidArgumentException::class);
        $storage = new LocalFileSystem($this->path_not_exist);
    }

    /**
     * @covers FileBuilder\Storage\LocalFileSystem
     **/
    public function testLocalFileSystemExceptionFileExists()
    {
        $storage = new LocalFileSystem($this->path);
        $this->expectException(Exception::class);
        $storage->save("fake_file.txt", "content");
    }

    /**
     * @covers FileBuilder\Storage\LocalFileSystem
     **/
    public function testLocalFileSystem()
    {
        $storage = new LocalFileSystem($this->path);
        $this->assertTrue($storage->save("saved.file", "This is the file content"));
        $this->assertFileExists($this->path . "saved.file");
        $this->assertFileEquals($this->path . "expected.file", $this->path . "saved.file");
        unlink($this->path . "saved.file");
    }
}
